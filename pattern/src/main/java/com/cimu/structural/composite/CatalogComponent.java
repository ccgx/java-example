package com.cimu.structural.composite;

/**
 * Title: CatalogComponent
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月18日 12:05
 */
public abstract class CatalogComponent {
    public String getName(CatalogComponent catalogComponent){
        throw new UnsupportedOperationException("不支持获取名称");
    }
    public void add(CatalogComponent catalogComponent){
        throw new UnsupportedOperationException("不支持添加");
    }
    public void remove(CatalogComponent catalogComponent){
        throw new UnsupportedOperationException("不支持移除");
    }
    public double getPrice(CatalogComponent catalogComponent){
        throw new UnsupportedOperationException("不支持获取价格");
    }
    public void show(){
        throw new UnsupportedOperationException("不支持展示");
    }
}
