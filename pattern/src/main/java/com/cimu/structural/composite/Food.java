package com.cimu.structural.composite;

/**
 * Title: Food
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月18日 12:09
 */
public class Food extends CatalogComponent {
    private String name;
    private double price;
    public Food(String name, double price) {
        this.name = name;
        this.price = price;
    }
    @Override
    public String getName(CatalogComponent catalogComponent) {
        return this.name;
    }
    @Override
    public double getPrice(CatalogComponent catalogComponent) {
        return this.price;
    }
    @Override
    public void show() {
        System.out.println("食物名称："+this.name+";价格"+this.price);
    }
}
