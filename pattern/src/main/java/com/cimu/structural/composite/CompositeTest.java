package com.cimu.structural.composite;

/**
 * Title: CompositeTest
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月18日 12:17
 */
public class CompositeTest {
    public static void main(String[] args) {
        CatalogComponent food = new Food("红烧肉",20);
        CatalogComponent food2 = new Food("红烧鸡",15);
        CatalogComponent meatCatalog = new MenuCatalog("荤菜系列");
        meatCatalog.add(food);
        meatCatalog.add(food2);

        CatalogComponent food3 = new Food("炒青菜",2);
        CatalogComponent food4 = new Food("干锅花菜",3);
        CatalogComponent soupCatalog = new MenuCatalog("蔬菜系列");
        soupCatalog.add(food3);
        soupCatalog.add(food4);

        CatalogComponent mainCatalog = new MenuCatalog("主菜单目录");
        mainCatalog.add(meatCatalog);
        mainCatalog.add(soupCatalog);
        mainCatalog.show();
    }
}
