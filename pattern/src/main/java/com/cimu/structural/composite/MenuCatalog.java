package com.cimu.structural.composite;

import java.util.ArrayList;
import java.util.List;

/**
 * Title: MenuCatalog
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月18日 12:10
 */
public class MenuCatalog extends CatalogComponent{
    private String name;
    private List<CatalogComponent> items = new ArrayList<CatalogComponent>();
    public MenuCatalog(String name) {
        this.name = name;
    }
    @Override
    public String getName(CatalogComponent catalogComponent) {
        return this.name;
    }
    @Override
    public void add(CatalogComponent catalogComponent) {
        items.add(catalogComponent);
    }
    @Override
    public void remove(CatalogComponent catalogComponent) {
        items.remove(catalogComponent);
    }
    @Override
    public void show() {
        System.out.println("菜单目录："+this.name);
        for(CatalogComponent catalogComponent : items){
            catalogComponent.show();
        }
    }
}
