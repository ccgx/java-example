package com.cimu.structural.bridge;

/**
 * Title: GoldVip
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月18日 19:04
 */
public class GoldVip implements Vip {
    @Override
    public Vip buyVip() {
        System.out.println("购买黄金会员");
        return new GoldVip();
    }
    @Override
    public void showVipType() {
        System.out.println("这是黄金会员");
    }
}
