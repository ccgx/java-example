package com.cimu.structural.bridge;

/**
 * Title: PcPlatform
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月18日 19:08
 */
public class PcPlatform extends Platform {
    @Override
    Vip buyVip() {
        return vip.buyVip();
    }
    public PcPlatform(Vip vip) {
        super(vip);
    }
}
