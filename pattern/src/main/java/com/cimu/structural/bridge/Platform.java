package com.cimu.structural.bridge;

/**
 * Title: Platform
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月18日 19:06
 */
public abstract class Platform {
    protected Vip vip;
    public Platform(Vip vip) {
        this.vip = vip;
    }
    abstract Vip buyVip();
}
