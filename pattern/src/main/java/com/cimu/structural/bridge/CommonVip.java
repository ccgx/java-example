package com.cimu.structural.bridge;

/**
 * Title: CommonVip
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月18日 19:03
 */
public class CommonVip implements Vip {
    @Override
    public Vip buyVip() {
        System.out.println("购买普通会员");
        return new CommonVip();
    }
    @Override
    public void showVipType() {
        System.out.println("这是普通会员");
    }
}
