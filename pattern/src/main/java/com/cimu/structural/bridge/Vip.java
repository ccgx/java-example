package com.cimu.structural.bridge;

/**
 * Title: Vip
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月18日 19:02
 */
public interface Vip {
    Vip buyVip();
    void showVipType();
}
