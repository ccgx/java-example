package com.cimu.structural.bridge;

/**
 * Title: BridgeTest
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月18日 19:10
 */
public class BridgeTest {
    public static void main(String[] args) {
        Platform pcPlatform = new PcPlatform(new CommonVip());
        Vip commonVip = pcPlatform.buyVip();
        commonVip.showVipType();

        Platform appPlatform = new AppPlatform(new GoldVip());
        Vip goldVip = appPlatform.buyVip();
        goldVip.showVipType();
    }
}
