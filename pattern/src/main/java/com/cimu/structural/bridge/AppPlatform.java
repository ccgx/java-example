package com.cimu.structural.bridge;

/**
 * Title: AppPlatform
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月18日 19:09
 */
public class AppPlatform extends Platform {
    public AppPlatform(Vip vip) {
        super(vip);
    }
    @Override
    Vip buyVip() {
        return vip.buyVip();
    }
}
