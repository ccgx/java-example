package com.cimu.structural.flyweight;

/**
 * Title: FlyweightTest
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月15日 12:29
 */
public class FlyweightTest {
    private static final String[] names = {"zhangsan","lisi","wangwu","zhaoliu"};
    public static void main(String[] args) {
        for(int i=0;i<10;i++){
            String name = names[(int)(Math.random() * names.length)];
            RDEmployee rdEmployee = (RDEmployee) EmployeeFactory.getEmployee(name);
            rdEmployee.readSummary();
        }

    }
}
