package com.cimu.structural.flyweight;

/**
 * Title: RDEmployee
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月15日 12:21
 */
public class RDEmployee implements Employee{
    //属于内部状态
    private String department = "研发中心";
    private String name;
    //属于外部状态
    private String summaryContent;
    public void writeSummary(String content){
        summaryContent=content;
    }
    public RDEmployee(String name) {
        this.name = name;
        System.out.println("创建员工"+name);
    }
    @Override
    public void readSummary() {
        System.out.println(summaryContent);
    }
}
