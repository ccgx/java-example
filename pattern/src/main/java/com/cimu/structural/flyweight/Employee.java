package com.cimu.structural.flyweight;

/**
 * Title: Employee
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月15日 12:16
 */
public interface Employee {
    void readSummary();
}
