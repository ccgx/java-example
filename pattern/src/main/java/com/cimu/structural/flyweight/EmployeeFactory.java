package com.cimu.structural.flyweight;

import java.util.HashMap;
import java.util.Map;

/**
 * Title: EmployeeFactory
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月15日 12:18
 */
public class EmployeeFactory {
    public final static Map<String,Employee> EMPLOYEE_MAP = new HashMap<String, Employee>();
    public static Employee getEmployee(String name){
        RDEmployee employee = (RDEmployee)EMPLOYEE_MAP.get(name);
        if(null == employee){
            employee = new RDEmployee(name);
            employee.writeSummary(name+"员工写的总结内容为。。。");
            EMPLOYEE_MAP.put(name,employee);
        }
        return employee;
    }
}
