package com.cimu.structural.decorator;

/**
 * Title: Coffee
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月13日 21:45
 */
public interface Coffee {
    int getCost();
    String getDesc();
}
