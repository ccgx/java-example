package com.cimu.structural.decorator;

/**
 * Title: AddMilkDecorator
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月13日 21:49
 */
public class AddMilkDecorator extends CoffeeDecorator {
    public AddMilkDecorator(Coffee coffee) {
        super(coffee);
    }
    @Override
    public int getCost() {
        return super.getCost()+2;
    }
    @Override
    public String getDesc() {
        return super.getDesc()+" 加点牛奶";
    }
}
