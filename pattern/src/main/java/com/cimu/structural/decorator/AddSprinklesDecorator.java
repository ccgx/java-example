package com.cimu.structural.decorator;

/**
 * Title: AddSprinklesDecorator
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月13日 21:50
 */
public class AddSprinklesDecorator extends CoffeeDecorator {
    public AddSprinklesDecorator(Coffee coffee) {
        super(coffee);
    }
    @Override
    public int getCost() {
        return super.getCost()+1;
    }
    @Override
    public String getDesc() {
        return super.getDesc()+" 加点糖";
    }
}
