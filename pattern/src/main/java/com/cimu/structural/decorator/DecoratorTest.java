package com.cimu.structural.decorator;

/**
 * Title: DecoratorTest
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月13日 21:51
 */
public class DecoratorTest {
    public static void main(String[] args) {
        Coffee coffee = new CoffeeImpl();
        coffee = new AddMilkDecorator(coffee);
        coffee = new AddSprinklesDecorator(coffee);
        System.out.println(coffee.getDesc()+"价格："+coffee.getCost());
    }
}
