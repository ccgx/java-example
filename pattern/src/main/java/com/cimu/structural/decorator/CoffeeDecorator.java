package com.cimu.structural.decorator;

/**
 * Title: CoffeeDecorator
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月13日 21:47
 */
public abstract class CoffeeDecorator implements Coffee {
    private Coffee coffee;
    public CoffeeDecorator(Coffee coffee) {
        this.coffee = coffee;
    }
    @Override
    public  int getCost() {
        return coffee.getCost();
    }
    @Override
    public String getDesc() {
        return coffee.getDesc();
    }
}
