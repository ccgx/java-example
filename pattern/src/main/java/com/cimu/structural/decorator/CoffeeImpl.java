package com.cimu.structural.decorator;

/**
 * Title: CoffeeImpl
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月13日 21:46
 */
public class CoffeeImpl implements Coffee {
    @Override
    public int getCost() {
        return 10;
    }
    @Override
    public String getDesc() {
        return "原味咖啡";
    }
}
