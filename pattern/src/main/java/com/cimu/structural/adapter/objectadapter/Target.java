package com.cimu.structural.adapter.objectadapter;

/**
 * Title: Target
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月14日 20:50
 */
public interface Target {
    void getData();
}
