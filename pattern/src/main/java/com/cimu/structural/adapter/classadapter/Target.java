package com.cimu.structural.adapter.classadapter;

/**
 * Title: Target
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月14日 20:38
 */
public interface Target {
    void getData();
}
