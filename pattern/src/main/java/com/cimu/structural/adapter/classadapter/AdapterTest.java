package com.cimu.structural.adapter.classadapter;

/**
 * Title: AdapterTest
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月14日 20:41
 */
public class AdapterTest {
    public static void main(String[] args) {
        Target target = new Adapter();
        target.getData();
    }
}
