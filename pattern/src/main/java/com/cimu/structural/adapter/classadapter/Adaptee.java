package com.cimu.structural.adapter.classadapter;

/**
 * Title: Adaptee
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月14日 20:39
 */
public class Adaptee {
    public void adapteeGetData(){
        System.out.println("被适配类方法");
    }
}
