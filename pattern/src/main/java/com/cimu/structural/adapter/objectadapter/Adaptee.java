package com.cimu.structural.adapter.objectadapter;

/**
 * Title: Adaptee
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月14日 20:50
 */
public class Adaptee {
    public void adapteeData(){
        System.out.println("被适配者方法");
    }
}
