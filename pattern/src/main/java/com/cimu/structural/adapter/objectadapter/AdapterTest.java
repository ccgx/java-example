package com.cimu.structural.adapter.objectadapter;

/**
 * Title: AdapterTest
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月14日 20:51
 */
public class AdapterTest {
    public static void main(String[] args) {
        Target target = new Adapter();
        target.getData();
    }
}
