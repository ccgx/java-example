package com.cimu.structural.adapter.objectadapter;

/**
 * Title: Adapter
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月14日 20:51
 */
public class Adapter implements Target {
    private Adaptee adaptee = new Adaptee();
    @Override
    public void getData() {
        adaptee.adapteeData();
    }
}
