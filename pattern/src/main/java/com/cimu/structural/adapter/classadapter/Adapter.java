package com.cimu.structural.adapter.classadapter;

/**
 * Title: Adapter
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月14日 20:41
 */
public class Adapter extends Adaptee implements Target {
    @Override
    public void getData() {
        super.adapteeGetData();
    }
}
