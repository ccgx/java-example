package com.cimu.structural.facadepattern;

/**
 * Title: Test
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月01日 11:59
 */
public class Test {
    public static void main(String[] args) {
        ShapeMaker shapeMaker = new ShapeMaker();
        shapeMaker.drawCircle();
        shapeMaker.drawRectangle();
    }
}
