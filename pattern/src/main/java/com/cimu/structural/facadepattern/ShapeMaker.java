package com.cimu.structural.facadepattern;

/**
 * Title: ShapeMaker
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月01日 11:56
 */
public class ShapeMaker {
    private Circle circle;
    private Rectangle rectangle;

    public ShapeMaker(){
        circle = new Circle();
        rectangle = new Rectangle();
    }

    public void drawCircle(){
        circle.draw();
    }

    public void drawRectangle(){
        rectangle.draw();
    }

}
