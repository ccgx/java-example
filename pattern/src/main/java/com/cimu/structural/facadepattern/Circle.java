package com.cimu.structural.facadepattern;

/**
 * Title: Circle
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月01日 11:55
 */
public class Circle implements Shape{
    @Override
    public void draw() {
        System.out.println("画圆");
    }
}
