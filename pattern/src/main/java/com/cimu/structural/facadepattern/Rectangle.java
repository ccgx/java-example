package com.cimu.structural.facadepattern;

/**
 * Title: Rectangle
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月01日 11:55
 */
public class Rectangle implements Shape{
    @Override
    public void draw() {
        System.out.println("画长方形");
    }
}
