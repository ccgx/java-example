package com.cimu.structural.facadepattern;

/**
 * Title: Shape
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月01日 09:09
 */
public interface Shape {
    void draw();
}
