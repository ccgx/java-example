package com.cimu.structural.proxy.staticproxy;

/**
 * Title: OrderServiceStaticProxy
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月19日 20:52
 */
public class OrderServiceStaticProxy {
    private OrderService orderService;
    public String saveOrder(Integer userId) {
        methodBefore();
        if(null == orderService){
            orderService = new OrderServiceImpl();
        }
        String orderNo = orderService.saveOrder(userId);
        methodAfter();
        return orderNo;
    }
    private void methodBefore(){
        System.out.println("订单前校验参数");
    }
    private void methodAfter(){
        System.out.println("订单完成之后做点什么事情");
    }
}
