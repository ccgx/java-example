package com.cimu.structural.proxy.dynamic;

/**
 * Title: Order
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月21日 13:02
 */
public class Order {
    private Integer userId;
    private String orderNo;
    public Integer getUserId() {
        return userId;
    }
    public void setUserId(Integer userId) {
        this.userId = userId;
    }
    public String getOrderNo() {
        return orderNo;
    }
    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }
    @Override
    public String toString() {
        return "Order{" +
                "userId=" + userId +
                ", orderNo='" + orderNo + '\'' +
                '}';
    }
}
