package com.cimu.structural.proxy.dynamic;


/**
 * Title: DynamicProxyTest
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月21日 13:29
 */
public class DynamicProxyTest {
    public static void main(String[] args) {
        Order order = new Order();
        order.setUserId(1);
        OrderService proxy = (OrderService) new OrderServiceDynamicProxy(new OrderServiceImpl()).bind();
        order = proxy.saveOrder(order);
        System.out.println("订单数据"+order);
    }
}
