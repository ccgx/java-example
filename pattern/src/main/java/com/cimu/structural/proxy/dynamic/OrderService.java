package com.cimu.structural.proxy.dynamic;

/**
 * Title: OrderService
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月21日 13:16
 */
public interface OrderService {
    Order saveOrder(Order order);
}
