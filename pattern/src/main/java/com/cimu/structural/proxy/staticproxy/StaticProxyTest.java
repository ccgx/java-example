package com.cimu.structural.proxy.staticproxy;

/**
 * Title: StaticProxyTest
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月19日 20:56
 */
public class StaticProxyTest {
    public static void main(String[] args) {
        OrderServiceStaticProxy orderServiceStaticProxy = new OrderServiceStaticProxy();
        orderServiceStaticProxy.saveOrder(1);
    }
}
