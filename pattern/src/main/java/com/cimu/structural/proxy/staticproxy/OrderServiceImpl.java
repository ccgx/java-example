package com.cimu.structural.proxy.staticproxy;

/**
 * Title: OrderServiceImpl
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月19日 20:49
 */
public class OrderServiceImpl implements OrderService{
    @Override
    public String saveOrder(Integer userId) {
        System.out.println("创建订单，用户id为:"+userId);
        return "sn123456";
    }
}
