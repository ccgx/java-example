package com.cimu.structural.proxy.staticproxy;

/**
 * Title: OrderService
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月19日 20:49
 */
public interface OrderService {
    String saveOrder(Integer userId);
}
