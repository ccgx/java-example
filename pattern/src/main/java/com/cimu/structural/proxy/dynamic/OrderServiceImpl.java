package com.cimu.structural.proxy.dynamic;

/**
 * Title: OrderServiceImpl
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月21日 13:17
 */
public class OrderServiceImpl implements OrderService{
    @Override
    public Order saveOrder(Order order) {
        order.setOrderNo("sn123");
        System.out.println("保存订单数据");
        return order;
    }
}
