package com.cimu.behavioral.observer.guava;

import com.google.common.eventbus.Subscribe;

/**
 * Title: GuavaWaiter
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月25日 12:02
 */
public class GuavaWaiter {
    @Subscribe
    public void servingGreens(Object obj){
        System.out.println(obj+"准备上菜");
    }
}
