package com.cimu.behavioral.observer;

/**
 * Title: ObserverTest
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月25日 11:14
 */
public class ObserverTest {
    public static void main(String[] args) {
        Cook cook = new Cook("李四");
        Waiter waiter1 = new Waiter("服务员1");
        Waiter waiter2 = new Waiter("服务员2");
        cook.addObserver(waiter1);
        cook.addObserver(waiter2);
        cook.burnGreens("番茄炒蛋");
    }
}
