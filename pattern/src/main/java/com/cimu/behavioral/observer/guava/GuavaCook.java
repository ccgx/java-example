package com.cimu.behavioral.observer.guava;

import com.google.common.eventbus.EventBus;

/**
 * Title: GuavaCook
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月25日 12:04
 */
public class GuavaCook {
    private EventBus eventBus = new EventBus();
    public GuavaCook() {
    }
    public void register(Object obj){
        eventBus.register(obj);
    }
    public void unregister(Object obj){
        eventBus.unregister(obj);
    }
    public void doneGreens(Object obj){
        eventBus.post(obj);
    }
}
