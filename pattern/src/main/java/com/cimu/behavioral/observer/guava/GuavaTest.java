package com.cimu.behavioral.observer.guava;

/**
 * Title: GuavaTest
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月25日 12:10
 */
public class GuavaTest {
    public static void main(String[] args) {
        GuavaWaiter guavaWaiter = new GuavaWaiter();
        GuavaCook guavaCook = new GuavaCook();
        guavaCook.register(guavaWaiter);
        guavaCook.doneGreens("番茄炒蛋");
    }
}
