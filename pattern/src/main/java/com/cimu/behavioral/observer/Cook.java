package com.cimu.behavioral.observer;

import java.util.Observable;

/**
 * Title: Cook
 * Copyright: Copyright (c) 2017
 * 厨师类
 * @author cgx
 * date 2019年07月25日 11:01
 */
public class Cook extends Observable {
    private String name;
    public Cook(String name) {
        this.name = name;
    }
    public String getName() {
        return name;
    }
    public void burnGreens(String greens){
        setChanged();
        notifyObservers(greens);
    }
}
