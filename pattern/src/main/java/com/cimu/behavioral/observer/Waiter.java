package com.cimu.behavioral.observer;

import java.util.Observable;
import java.util.Observer;

/**
 * Title: Waiter
 * Copyright: Copyright (c) 2017
 * 服务员
 * @author cgx
 * date 2019年07月25日 11:02
 */
public class Waiter implements Observer {
    private String name;
    public Waiter(String name) {
        this.name = name;
    }
    @Override
    public void update(Observable o, Object arg) {
        String greens = (String)arg;
        Cook cook = (Cook)o;
        System.out.println(cook.getName()+"厨师烧了一个"+greens+"，"+this.name+"准备上菜");
    }
}
