package com.cimu.behavioral.interpreter;

import java.util.HashMap;
import java.util.Map;

/**
 * Title: InterpreterTest
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月24日 15:03
 */
public class InterpreterTest {
    public static void main(String[] args) {
        Interpreter.Expr expr = Interpreter.parse("w x z - +");
        Map<String, Integer> context = new HashMap<>();
        context.put("w",5);
        context.put("x",10);
        context.put("z",42);
        int result = expr.interpret(context);
        // -27
        System.out.println(result);
    }
}
