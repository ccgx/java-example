package com.cimu.behavioral.mediator;

/**
 * Title: MediatorTest
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月26日 13:22
 */
public class MediatorTest {
    public static void main(String[] args) {
        Mediator<Integer> mediator = new Mediator<>();
        mediator.setValue("bob", 20);
        mediator.setValue("alice", 24);
        mediator.getValue("alice").ifPresent(age -> System.out.println("age for alice: " + age));

        mediator.addObserver("bob", () -> {
            System.out.println("new age for bob: " + mediator.getValue("bob").orElseThrow(RuntimeException::new));
        });
        mediator.setValue("bob", 21);
    }
}
