package com.cimu.behavioral.mediator;

/**
 * Title: Storage
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月26日 13:21
 */
public class Storage<T> {
    T value;
    T getValue() {
        return value;
    }
    void setValue(Mediator<T> mediator, String storageName, T value) {
        this.value = value;
        mediator.notifyObservers(storageName);
    }
}
