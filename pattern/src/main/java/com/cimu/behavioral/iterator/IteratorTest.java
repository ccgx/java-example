package com.cimu.behavioral.iterator;

/**
 * Title: IteratorTest
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月22日 16:42
 */
public class IteratorTest {
    public static void main(String[] args) {
        Order order1 = new Order("订单1");
        Order order2 = new Order("订单2");
        Order order3 = new Order("订单3");
        Order order4 = new Order("订单4");
        OrderAggregate orderAggregate = new OrderAggregateImpl();
        orderAggregate.addOrder(order1);
        orderAggregate.addOrder(order2);
        orderAggregate.addOrder(order3);
        orderAggregate.addOrder(order4);
        OrderIterator orderIterator = orderAggregate.getOrderIterator();
        while (!orderIterator.isLastOrder()){
            Order orderDb = orderIterator.nextOrder();
            System.out.println(orderDb.getName());
        }
    }
}
