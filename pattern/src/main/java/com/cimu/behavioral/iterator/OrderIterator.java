package com.cimu.behavioral.iterator;

/**
 * Title: OrderIterator
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月22日 16:34
 */
public interface OrderIterator {
    Order nextOrder();
    boolean isLastOrder();
}
