package com.cimu.behavioral.iterator;

import java.util.ArrayList;
import java.util.List;

/**
 * Title: OrderAggregateImpl
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月22日 16:37
 */
public class OrderAggregateImpl implements OrderAggregate {
    private List orderList;
    public OrderAggregateImpl() {
        this.orderList = new ArrayList();
    }
    @Override
    public void addOrder(Order order) {
        orderList.add(order);
    }
    @Override
    public void removeOrder(Order order) {
        orderList.remove(order);
    }
    @Override
    public OrderIterator getOrderIterator() {
        return new OrderIteratorImpl(orderList);
    }
}
