package com.cimu.behavioral.iterator;

/**
 * Title: Order
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月22日 16:32
 */
public class Order {
    private String name;
    public Order(String name) {
        this.name = name;
    }
    public String getName() {
        return name;
    }
}
