package com.cimu.behavioral.iterator;

import java.util.List;

/**
 * Title: OrderIteratorImpl
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月22日 16:36
 */
public class OrderIteratorImpl implements OrderIterator{
    private List orderList;
    private int position;
    private Order order;
    public OrderIteratorImpl(List orderList) {
        this.orderList = orderList;
    }
    @Override
    public Order nextOrder() {
        System.out.println("订单位置: "+position);
        order = (Order) orderList.get(position);
        position++;
        return order;
    }
    @Override
    public boolean isLastOrder() {
        return position >= orderList.size();
    }
}
