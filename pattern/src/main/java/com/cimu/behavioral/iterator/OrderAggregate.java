package com.cimu.behavioral.iterator;

/**
 * Title: OrderAggregate
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月22日 16:34
 */
public interface OrderAggregate {
    void addOrder(Order order);
    void removeOrder(Order order);
    OrderIterator getOrderIterator();
}
