package com.cimu.behavioral.memento;

/**
 * Title: MementoTest
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月25日 15:34
 */
public class MementoTest {
    public static void main(String[] args) {
        Game game = new Game(1,2);
        Game game2 = new Game(2,3);
        GameManage gameManage = new GameManage();
        gameManage.addMemento(game.saveToMemento());
        gameManage.addMemento(game2.saveToMemento());

        game.restoreFromMemento(gameManage.getGameMemento());
        game2.restoreFromMemento(gameManage.getGameMemento());
    }
}
