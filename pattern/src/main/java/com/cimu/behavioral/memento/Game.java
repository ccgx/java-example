package com.cimu.behavioral.memento;

/**
 * Title: Game
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月25日 13:25
 */
public class Game {
    private int level;
    private int progress;
    public Game(int level, int progress) {
        this.level = level;
        this.progress = progress;
    }
    public GameMemento saveToMemento(){
        System.out.println("当前等级到"+level+",当前进度:"+progress);
        return new GameMemento(level,progress);
    }
    public void restoreFromMemento(GameMemento gameMemento){
        this.level = gameMemento.getLevel();
        this.progress = gameMemento.getProgress();
        System.out.println("恢复等级到"+this.level+",恢复进度:"+this.progress);
    }
}
