package com.cimu.behavioral.memento;

/**
 * Title: Game
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月25日 13:25
 */
public class GameMemento {
    private int level;
    private int progress;
    public GameMemento(int level, int progress) {
        this.level = level;
        this.progress = progress;
    }
    public int getLevel() {
        return level;
    }
    public int getProgress() {
        return progress;
    }
}
