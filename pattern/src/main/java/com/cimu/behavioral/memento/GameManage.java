package com.cimu.behavioral.memento;

import java.util.Stack;

/**
 * Title: GameManage
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月25日 15:30
 */
public class GameManage {
    private static Stack<GameMemento> stack = new Stack<>();
    public void addMemento(GameMemento gameMemento){
        stack.push(gameMemento);
    }
    public GameMemento getGameMemento(){
        return stack.pop();
    }
}
