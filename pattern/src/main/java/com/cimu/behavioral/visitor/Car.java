package com.cimu.behavioral.visitor;

import java.util.ArrayList;
import java.util.List;

/**
 * Title: Car
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月28日 10:15
 */
public class Car implements CarElement{
    private List<CarElement> elements;
    public Car() {
        this.elements = new ArrayList<>();
        this.elements.add(new Wheel("前左"));
        this.elements.add(new Wheel("前右"));
        this.elements.add(new Wheel("后左"));
        this.elements.add(new Wheel("后右"));
        this.elements.add(new Body());
        this.elements.add(new Engine());
    }
    @Override
    public void accept(CarElementVisitor visitor) {
        for (CarElement element : elements) {
            element.accept(visitor);
        }
        visitor.visit(this);
    }
}
