package com.cimu.behavioral.visitor;

/**
 * Title: CarElementPrintVisitor
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月28日 10:20
 */
public class CarElementPrintVisitor implements CarElementVisitor{
    @Override
    public void visit(Body body) {
        System.out.println("访问车身");
    }
    @Override
    public void visit(Car car) {
        System.out.println("访问汽车");
    }
    @Override
    public void visit(Engine engine) {
        System.out.println("访问引擎");
    }
    @Override
    public void visit(Wheel wheel) {
        System.out.println("访问 " + wheel.getName() + " 车轮");
    }
}
