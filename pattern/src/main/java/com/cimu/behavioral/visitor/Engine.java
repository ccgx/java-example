package com.cimu.behavioral.visitor;

/**
 * Title: Engine
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月28日 10:14
 */
public class Engine implements CarElement{
    @Override
    public void accept(CarElementVisitor visitor) {
        visitor.visit(this);
    }
}
