package com.cimu.behavioral.visitor;

/**
 * Title: VisitorTest
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月28日 10:21
 */
public class VisitorTest {
    public static void main(String[] args) {
        Car car = new Car();
        car.accept(new CarElementPrintVisitor());
        car.accept(new CarElementDoVisitor());
    }
}
