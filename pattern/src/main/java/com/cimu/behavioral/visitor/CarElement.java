package com.cimu.behavioral.visitor;

/**
 * Title: CarElement
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月28日 10:12
 */
public interface CarElement {
    void accept(CarElementVisitor visitor);
}
