package com.cimu.behavioral.visitor;
/**
 * Title: CarElementVisitor
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月28日 10:13
 */
public interface CarElementVisitor {
    void visit(Body body);
    void visit(Car car);
    void visit(Engine engine);
    void visit(Wheel wheel);
}
