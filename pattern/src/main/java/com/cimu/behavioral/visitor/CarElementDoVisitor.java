package com.cimu.behavioral.visitor;

/**
 * Title: CarElementDoVisitor
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月28日 10:18
 */
public class CarElementDoVisitor implements CarElementVisitor{
    @Override
    public void visit(Body body) {
        System.out.println("移动车身");
    }
    @Override
    public void visit(Car car) {
        System.out.println("启动车子");
    }
    @Override
    public void visit(Wheel wheel) {
        System.out.println("启动我的 " + wheel.getName() + " 车轮");
    }
    @Override
    public void visit(Engine engine) {
        System.out.println("开启引擎");
    }
}
