package com.cimu.behavioral.visitor;

/**
 * Title: Wheel
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月28日 10:13
 */
public class Wheel implements CarElement {
    private final String name;
    public Wheel(final String name) {
        this.name = name;
    }
    public String getName() {
        return name;
    }
    @Override
    public void accept(CarElementVisitor visitor) {
        visitor.visit(this);
    }
}
