package com.cimu.behavioral.state;

/**
 * Title: StateTest
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月28日 10:58
 */
public class StateTest {
    public static void main(String[] args) {
        StateContext context = new StateContext();
        context.writeName("Monday");
        context.writeName("Tuesday");
        context.writeName("Wednesday");
        context.writeName("Thursday");
        context.writeName("Friday");
        context.writeName("Saturday");
        context.writeName("Sunday");
    }
}
