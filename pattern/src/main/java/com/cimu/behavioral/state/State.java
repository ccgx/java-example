package com.cimu.behavioral.state;

/**
 * Title: State
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月28日 10:56
 */
public interface State {
    void writeName(StateContext context, String name);
}
