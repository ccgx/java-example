package com.cimu.behavioral.state;

/**
 * Title: StateContext
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月28日 10:56
 */
public class StateContext {
    private State state;
    public StateContext() {
        state = new LowerCaseState();
    }
    void setState(State newState) {
        //改变状态
        state = newState;
    }
    public void writeName(String name) {
        state.writeName(this, name);
    }
}
