package com.cimu.behavioral.state;

/**
 * Title: LowerCaseState
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月28日 10:57
 */
public class LowerCaseState implements State{
    @Override
    public void writeName(StateContext context, String name) {
        System.out.println(name.toLowerCase());
        //输出一个小写之后变大写
        context.setState(new MultipleUpperCaseState());
    }
}
