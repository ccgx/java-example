package com.cimu.behavioral.state;

/**
 * Title: MultipleUpperCaseState
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月28日 10:57
 */
public class MultipleUpperCaseState implements State{
    //计数
    private int count = 0;
    @Override
    public void writeName(StateContext context, String name) {
        System.out.println(name.toUpperCase());
        //转换到2个之后变成小写
        if(++count > 1) {
            context.setState(new LowerCaseState());
        }
    }
}
