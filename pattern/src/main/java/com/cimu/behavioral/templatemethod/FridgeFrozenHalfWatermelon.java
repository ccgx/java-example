package com.cimu.behavioral.templatemethod;

/**
 * Title: FridgeFrozenHalfWatermelon
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月21日 22:27
 */
public class FridgeFrozenHalfWatermelon extends AFridgeFrozen {
    @Override
    void putThings() {
        System.out.println("放一半西瓜");
    }
    @Override
    protected boolean needPackThing() {
        return true;
    }
}
