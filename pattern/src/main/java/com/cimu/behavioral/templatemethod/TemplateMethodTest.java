package com.cimu.behavioral.templatemethod;

/**
 * Title: TemplateMethodTest
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月21日 22:25
 */
public class TemplateMethodTest {
    public static void main(String[] args) {
        AFridgeFrozen fridgeFrozen = new FridgeFrozenAgg();
        fridgeFrozen.fridgeFrozen();
        System.out.println("--------------华丽的分割线---------------------");
        AFridgeFrozen watermelon = new FridgeFrozenHalfWatermelon();
        watermelon.fridgeFrozen();
    }
}
