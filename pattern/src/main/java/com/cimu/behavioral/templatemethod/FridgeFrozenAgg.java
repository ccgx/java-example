package com.cimu.behavioral.templatemethod;

/**
 * Title: FridgeFrozenAgg
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月21日 22:24
 */
public class FridgeFrozenAgg extends AFridgeFrozen {
    @Override
    void putThings() {
        System.out.println("放鸡蛋");
    }
}
