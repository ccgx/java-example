package com.cimu.behavioral.templatemethod;

/**
 * Title: AFridgeFrozen
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月21日 22:17
 */
public abstract class AFridgeFrozen {
    protected final void fridgeFrozen(){
        openFridge();
        if(needPackThing()){
            packThing();
        }
        putThings();
        closeFridge();
    }
    final void openFridge(){
        System.out.println("打开冰箱门");
    }
    /**
     *  放东西不一样
     */
    abstract void putThings();
    final void closeFridge(){
        System.out.println("关闭冰箱门");
    }
    final void packThing(){
        System.out.println("对放入的东西进行打包");
    }
    /**
     * 钩子方法，需要的子类自己实现
     */
    protected boolean needPackThing(){
        return false;
    }
}
