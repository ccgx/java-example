package com.cimu.behavioral.strategy;

/**
 * Title: EmptyStrategy
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月23日 09:01
 */
public class EmptyStrategy implements OrderStrategy {
    @Override
    public OrderTypeEnum supportType() {
        return null;
    }
    @Override
    public void saveOrder() {
        System.out.println("没有支付方式");
    }
}
