package com.cimu.behavioral.strategy;

/**
 * Title: WxpayStrategy
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月23日 08:49
 */
public class WxpayStrategy implements OrderStrategy{
    @Override
    public OrderTypeEnum supportType() {
        return OrderTypeEnum.WXPAY;
    }
    @Override
    public void saveOrder() {
        System.out.println("微信支付完成");
    }
}
