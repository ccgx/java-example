package com.cimu.behavioral.strategy;

/**
 * Title: OrderTypeEnum
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月23日 08:55
 */
public enum OrderTypeEnum {
    /**
     * 支付宝、微信
     */
    ALIPAY,WXPAY
}
