package com.cimu.behavioral.strategy;

/**
 * Title: OrderStrategy
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月23日 08:48
 */
public interface OrderStrategy {
    OrderTypeEnum supportType();
    void saveOrder();
}
