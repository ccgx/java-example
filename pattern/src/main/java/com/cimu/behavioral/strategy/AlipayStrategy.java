package com.cimu.behavioral.strategy;

/**
 * Title: AlipayStrategy
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月23日 08:48
 */
public class AlipayStrategy implements OrderStrategy {
    @Override
    public OrderTypeEnum supportType() {
        return OrderTypeEnum.ALIPAY;
    }
    @Override
    public void saveOrder() {
        System.out.println("支付宝支付完成");
    }
}
