package com.cimu.behavioral.strategy;

import java.util.List;

/**
 * Title: OrderFactory
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月23日 08:51
 */
public class OrderFactory {
    private OrderFactory(){
    }
    /** spring下通过spring来获取所有的bean */
    private static OrderStrategy[] strategies = new OrderStrategy[]{new AlipayStrategy(),new WxpayStrategy()};
    private static OrderStrategy EMPTY_STRATEGY = new EmptyStrategy();
    private static OrderStrategy getOrderStrategy(String orderType) {
        for (OrderStrategy handler : strategies) {
            if (handler.supportType().name().equals(orderType)) {
                return handler;
            }
        }
        return EMPTY_STRATEGY;
    }
    public static void saveOrder(String orderType){
        getOrderStrategy(orderType).saveOrder();
    }
}
