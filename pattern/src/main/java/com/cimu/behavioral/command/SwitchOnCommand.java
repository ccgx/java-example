package com.cimu.behavioral.command;

/**
 * Title: SwitchOnCommand
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月25日 17:17
 */
//command接口实现类
public class SwitchOnCommand implements Command {
    private Light light;
    public SwitchOnCommand(Light light) {
        this.light = light;
    }
    @Override
    public void execute() {
        light.turnOn();
    }
}
