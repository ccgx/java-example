package com.cimu.behavioral.command;

/**
 * Title: Command
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月25日 16:45
 */
//command接口
public interface Command {
    void execute();
}
