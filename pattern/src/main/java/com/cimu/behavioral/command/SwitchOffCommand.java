package com.cimu.behavioral.command;

/**
 * Title: SwitchOffCommand
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月25日 17:18
 */
//command接口实现类
public class SwitchOffCommand implements Command{
    private Light light;
    public SwitchOffCommand(Light light) {
        this.light = light;
    }
    @Override
    public void execute() {
        light.turnOff();
    }
}
