package com.cimu.behavioral.command;

import java.util.HashMap;

/**
 * Title: Switch
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月25日 17:15
 */
//Invoker
public class Switch {
    private final HashMap<String, Command> commandMap = new HashMap<>();
    public void register(String commandName, Command command) {
        commandMap.put(commandName, command);
    }
    public void execute(String commandName) {
        Command command = commandMap.get(commandName);
        if (command == null) {
            throw new IllegalStateException("no command registered for " + commandName);
        }
        command.execute();
    }
}
