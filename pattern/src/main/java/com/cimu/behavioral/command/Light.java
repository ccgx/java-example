package com.cimu.behavioral.command;

/**
 * Title: Light
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月25日 17:15
 */
//Receiver类
public class Light {
    public void turnOn(){
        System.out.println("开灯");
    }
    public void turnOff(){
        System.out.println("关灯");
    }
}
