package com.cimu.behavioral.command;

/**
 * Title: CommandTest
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月25日 17:43
 */
//client类
public class CommandTest {
    public static void main(String[] args) {
        Light lamp = new Light();
        Command switchOn = new SwitchOnCommand(lamp);
        Command switchOff = new SwitchOffCommand(lamp);
        Switch mySwitch = new Switch();
        mySwitch.register("on", switchOn);
        mySwitch.register("off", switchOff);
        mySwitch.execute("on");
        mySwitch.execute("off");
    }
}
