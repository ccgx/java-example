package com.cimu.behavioral.chainofresponsibility;

/**
 * Title: CEOApprove
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月26日 22:07
 */
public class CEOApprove extends Approve {
    @Override
    public void pass(Staff staff) {
        if(staff.getLeaveDays()>2){
            System.out.println(staff.getName()+"的请假,ceo批准了");
            if(null != approve){
                approve.pass(staff);
            }
        }else{
            System.out.println(staff.getName()+"找死逗我玩呢");
        }
    }
}
