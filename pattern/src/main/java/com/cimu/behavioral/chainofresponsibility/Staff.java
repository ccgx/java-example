package com.cimu.behavioral.chainofresponsibility;

/**
 * Title: Staff
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月26日 21:54
 */
public class Staff {
    public String name;
    /**
     * 请假天数
     */
    public Integer leaveDays;
    public Staff() {
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public Integer getLeaveDays() {
        return leaveDays;
    }
    public void setLeaveDays(Integer leaveDays) {
        this.leaveDays = leaveDays;
    }
}
