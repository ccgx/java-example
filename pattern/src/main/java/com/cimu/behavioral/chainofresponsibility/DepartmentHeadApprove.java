package com.cimu.behavioral.chainofresponsibility;

/**
 * Title: DepartmentHeadApprove
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月26日 22:01
 */
public class DepartmentHeadApprove extends Approve {
    @Override
    public void pass(Staff staff) {
        if(null != staff.getLeaveDays()){
            //请假天数不为空，进入审批流程
            System.out.println(staff.getName()+"的请假,部门领导批准了");
            if(staff.getLeaveDays()>2 && null != approve){
                //请假天数大于2天，需要更高级别的人审批
                approve.pass(staff);
            }
        }else{
            System.out.println(staff.getName()+"找死逗我玩呢");
        }
    }
}
