package com.cimu.behavioral.chainofresponsibility;

/**
 * Title: ChainTest
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月26日 22:09
 */
public class ChainTest {
    public static void main(String[] args) {
        Staff staff = new Staff();
        staff.setName("李四");
        staff.setLeaveDays(3);
        Approve departmentHeadApprove = new DepartmentHeadApprove();
        CEOApprove ceoApprove = new CEOApprove();
        departmentHeadApprove.setNextApprove(ceoApprove);
        departmentHeadApprove.pass(staff);
    }
}