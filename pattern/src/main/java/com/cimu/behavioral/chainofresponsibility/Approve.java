package com.cimu.behavioral.chainofresponsibility;

/**
 * Title: Approve
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月26日 21:55
 */
public abstract class Approve {
    protected Approve approve;
    public void setNextApprove(Approve approve){
        this.approve = approve;
    }
    public abstract void pass(Staff staff);
}
