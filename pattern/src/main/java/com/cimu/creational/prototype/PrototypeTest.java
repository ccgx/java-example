package com.cimu.creational.prototype;

import java.util.Date;

/**
 * Title: PrototypeTest
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月13日 20:36
 */
public class PrototypeTest {
    public static void main(String[] args) throws CloneNotSupportedException {
        Date now = new Date();
        Horse horse = new Horse("白色",now);
        Horse horse2 = (Horse) horse.clone();
        horse.getAge().setTime(444444444444L);
        System.out.println(horse);
        System.out.println(horse2);
    }

//    public static void main(String[] args) throws CloneNotSupportedException {
//        Date now = new Date();
//        Horse horse = new Horse("白色",now);
//        Horse horse2 = (Horse) horse.clone();
//        System.out.println(horse);
//        System.out.println(horse2);
//    }
}
