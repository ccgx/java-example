package com.cimu.creational.prototype;

import java.util.Date;

/**
 * Title: Horse
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月13日 20:33
 */
public class Horse implements Cloneable  {
    private String color;
    private Date age;
    public Horse(String color, Date age) {
        this.color = color;
        this.age = age;
    }
    public String getColor() {
        return color;
    }
    public void setColor(String color) {
        this.color = color;
    }
    public Date getAge() {
        return age;
    }
    public void setAge(Date age) {
        this.age = age;
    }
    @Override
    public String toString() {
        return "Horse{" +
                "color='" + color + '\'' +
                ", age=" + age +
                '}';
    }
//    @Override
//    protected Object clone() throws CloneNotSupportedException {
//        return super.clone();
//    }
    @Override
    protected Object clone() throws CloneNotSupportedException {
        Horse horse = (Horse) super.clone();
        //对时间进行深克隆
        horse.age = (Date) horse.age.clone();
        return horse;
    }
}
