package com.cimu.creational.simplefactory;

/**
 * Title: FoxconnFactory
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月02日 12:20
 */
public class FoxconnFactory {
    public Mobile getMobile(String mobileType){
        if("iphone".equals(mobileType)){
            return new IphoneMobile();
        }else if("huawei".equals(mobileType)){
            return new HuaweiMobile();
        }
        return null;
    }

    public Mobile getMobile(Class c){
        try {
            return (Mobile)Class.forName(c.getName()).newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }
}
