package com.cimu.creational.simplefactory;

/**
 * Title: SimpleFactoryTest
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月02日 12:15
 */
public class SimpleFactoryTest {
    public static void main(String[] args) {
        FoxconnFactory foxconnFactory = new FoxconnFactory();
        Mobile mobile = foxconnFactory.getMobile("iphone");
        mobile.produce();
        Mobile huawei = foxconnFactory.getMobile("huawei");
        huawei.produce();

        Mobile ipohoneMobile = foxconnFactory.getMobile(IphoneMobile.class);
        ipohoneMobile.produce();
    }
}
