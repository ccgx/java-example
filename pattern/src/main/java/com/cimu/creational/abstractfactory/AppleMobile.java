package com.cimu.creational.abstractfactory;

/**
 * Title: AppleMobile
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月08日 08:57
 */
public class AppleMobile extends Mobile{
    @Override
    public void produce() {
        System.out.println("生产苹果手机");
    }
}
