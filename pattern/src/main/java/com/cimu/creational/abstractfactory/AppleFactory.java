package com.cimu.creational.abstractfactory;

/**
 * Title: AppleFactory
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月08日 08:56
 */
public class AppleFactory implements FoxconnFactory{
    @Override
    public Mobile produceMobile() {
        return new AppleMobile();
    }
    @Override
    public Pad producePad() {
        return new ApplePad();
    }
}
