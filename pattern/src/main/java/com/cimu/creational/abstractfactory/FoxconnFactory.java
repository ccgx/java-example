package com.cimu.creational.abstractfactory;

/**
 * Title: FoxconnFactory
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月08日 08:53
 */
public interface FoxconnFactory {
    Mobile produceMobile();
    Pad producePad();
}
