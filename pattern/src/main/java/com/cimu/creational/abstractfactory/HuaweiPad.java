package com.cimu.creational.abstractfactory;

/**
 * Title: HuaweiPad
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月08日 09:00
 */
public class HuaweiPad extends Pad {
    @Override
    public void produce() {
        System.out.println("生产华为pad");
    }
}
