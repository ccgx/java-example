package com.cimu.creational.abstractfactory;

/**
 * Title: AbstractFactoryTest
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月08日 09:01
 */
public class AbstractFactoryTest {
    public static void main(String[] args) {
        FoxconnFactory appleFactory = new AppleFactory();
        Mobile appleMobile = appleFactory.produceMobile();
        appleMobile.produce();
        Pad applePad = appleFactory.producePad();
        applePad.produce();

        FoxconnFactory huaweiFactory = new HuaweiFactory();
        Mobile huaweiMobile = huaweiFactory.produceMobile();
        huaweiMobile.produce();
        Pad huaweiPad = huaweiFactory.producePad();
        huaweiPad.produce();
    }
}
