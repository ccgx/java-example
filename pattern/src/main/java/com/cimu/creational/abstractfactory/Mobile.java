package com.cimu.creational.abstractfactory;

/**
 * Title: Mobile
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月08日 08:54
 */
public abstract class Mobile {
    public abstract void produce();
}
