package com.cimu.creational.abstractfactory;

/**
 * Title: Pad
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月08日 08:53
 */
public abstract class Pad {
    public abstract void produce();
}
