package com.cimu.creational.abstractfactory;

/**
 * Title: HuaweiMobile
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月08日 08:59
 */
public class HuaweiMobile extends Mobile {
    @Override
    public void produce() {
        System.out.println("生产华为手机");
    }
}
