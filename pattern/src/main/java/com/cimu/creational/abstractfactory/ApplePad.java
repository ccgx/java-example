package com.cimu.creational.abstractfactory;

/**
 * Title: ApplePad
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月08日 08:58
 */
public class ApplePad extends Pad {
    @Override
    public void produce() {
        System.out.println("生产苹果pad");
    }
}
