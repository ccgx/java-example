package com.cimu.creational.abstractfactory;

/**
 * Title: HuaweiFactory
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月08日 08:56
 */
public class HuaweiFactory implements FoxconnFactory{
    @Override
    public Mobile produceMobile() {
        return new HuaweiMobile();
    }
    @Override
    public Pad producePad() {
        return new HuaweiPad();
    }
}
