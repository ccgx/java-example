package com.cimu.creational.singleton;

import java.io.Serializable;

/**
 * Title: HungrySerializableSingleton
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月12日 21:54
 */
public class HungrySerializableSingleton implements Serializable {
    private static HungrySerializableSingleton hungrySingleton = new HungrySerializableSingleton();
    private HungrySerializableSingleton(){
    }
    public static HungrySerializableSingleton getInstance(){
        return hungrySingleton;
    }
    private Object readResolve(){
        return hungrySingleton;
    }
}
