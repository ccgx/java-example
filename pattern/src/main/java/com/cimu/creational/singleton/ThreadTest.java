package com.cimu.creational.singleton;

/**
 * Title: ThreadTest
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月11日 19:38
 */
public class ThreadTest {
    public static void main(String[] args) {
        Thread t1 = new Thread(new MyRunnable());
        Thread t2 = new Thread(new MyRunnable());
        t1.start();
        t2.start();
    }
}
