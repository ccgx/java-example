package com.cimu.creational.singleton;

/**
 * Title: EnumSingleton
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月13日 10:08
 */
public enum EnumSingleton {
    INSTANCE;
    public static EnumSingleton getInstance(){
        return INSTANCE;
    }
}
