package com.cimu.creational.singleton;

import java.util.HashMap;
import java.util.Map;

/**
 * Title: ContainerSingleton
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月13日 10:30
 */
public class ContainerSingleton {
    private ContainerSingleton(){}
    private static Map<String,Object> singletonMap = new HashMap<String, Object>();
    public static void putInstance(String key,Object object){
        if(null != key && !"".equals(key) && null != object){
            if(!singletonMap.containsKey(key)){
                singletonMap.put(key,object);
            }
        }
    }
    public static Object getInstance(String key){
        return singletonMap.get(key);
    }
}
