package com.cimu.creational.singleton;

/**
 * Title: LazySingleton
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月11日 19:31
 */
public class LazySingleton {
   private static LazySingleton lazySingleton;
   private LazySingleton(){
   }
   public static LazySingleton getInstance(){
       if(null == lazySingleton){
           lazySingleton = new LazySingleton();
       }
       return lazySingleton;
   }
}
