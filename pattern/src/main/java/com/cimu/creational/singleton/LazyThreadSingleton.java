package com.cimu.creational.singleton;

/**
 * Title: LazyThreadSingleton
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月11日 19:57
 */
public class LazyThreadSingleton {
    private static LazyThreadSingleton lazyThreadSingleton;
    private LazyThreadSingleton(){}
    public synchronized static LazyThreadSingleton getInstance(){
        if(null == lazyThreadSingleton){
            lazyThreadSingleton = new LazyThreadSingleton();
        }
        return lazyThreadSingleton;
    }
}
