package com.cimu.creational.singleton;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Constructor;

/**
 * Title: SingletonTest
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月11日 19:32
 */
public class SingletonTest {

    public static void main(String[] args) {
        ContainerSingleton.putInstance("object",EnumSingleton.getInstance());
        System.out.println(ContainerSingleton.getInstance("object"));
    }

//    public static void main(String[] args) throws Exception{
////        EnumSingleton enumSingleton = EnumSingleton.getInstance();
////        System.out.println(enumSingleton);
//
////        Class objectClass = EnumSingleton.class;
////        Constructor declaredConstructors = objectClass.getDeclaredConstructor(String.class,int.class);
////        declaredConstructors.setAccessible(true);
////        EnumSingleton instance = EnumSingleton.getInstance();
////        System.out.println(instance);
////        EnumSingleton newInstance = (EnumSingleton) declaredConstructors.newInstance();
////        System.out.println(newInstance);
////        System.out.println(instance == newInstance);
//
//        EnumSingleton instance = EnumSingleton.getInstance();
//        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("instance"));
//        oos.writeObject(instance);
//        File file = new File("instance");
//        ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file));
//        EnumSingleton newInstance = (EnumSingleton) ois.readObject();
//        System.out.println(instance);
//        System.out.println(newInstance);
//        System.out.println(instance == newInstance);
//    }

//    public static void main(String[] args) throws Exception {
//        Class objectClass = HungrySingleton.class;
//        Constructor declaredConstructors = objectClass.getDeclaredConstructor();
//        declaredConstructors.setAccessible(true);
//        HungrySingleton instance = HungrySingleton.getInstance();
//        System.out.println(instance);
//        HungrySingleton newInstance = (HungrySingleton) declaredConstructors.newInstance();
//        System.out.println(newInstance);
//        System.out.println(instance == newInstance);
//    }

//    public static void main(String[] args) throws Exception {
//        HungrySerializableSingleton instance = HungrySerializableSingleton.getInstance();
//        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("instance"));
//        oos.writeObject(instance);
//        File file = new File("instance");
//        ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file));
//        HungrySerializableSingleton newInstance = (HungrySerializableSingleton) ois.readObject();
//        System.out.println(instance);
//        System.out.println(newInstance);
//        System.out.println(instance == newInstance);
//    }

//    public static void main(String[] args) {
//        StaticInnerSingleton staticInnerSingleton = StaticInnerSingleton.getInstance();
//        System.out.println(staticInnerSingleton);
//    }

//    public static void main(String[] args) {
//        LazyDoubleCheckSingleton lazyDoubleCheckSingleton = LazyDoubleCheckSingleton.getInstance();
//        System.out.println(lazyDoubleCheckSingleton);
//    }

//    public static void main(String[] args) {
//        LazySingleton lazySingleton = LazySingleton.getInstance();
//        System.out.println(lazySingleton);
//    }
}
