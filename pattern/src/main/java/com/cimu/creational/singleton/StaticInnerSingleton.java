package com.cimu.creational.singleton;

import java.io.Serializable;

/**
 * Title: StaticInnerSingleton
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月12日 21:29
 */
public class StaticInnerSingleton implements Serializable {
    private StaticInnerSingleton(){
    }
    private static class InnerClass {
        private static StaticInnerSingleton staticInnerSingleton = new StaticInnerSingleton();
    }
    public static StaticInnerSingleton getInstance(){
        return InnerClass.staticInnerSingleton;
    }
}
