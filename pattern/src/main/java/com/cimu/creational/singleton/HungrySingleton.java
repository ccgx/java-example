package com.cimu.creational.singleton;

/**
 * Title: HungrySingleton
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月12日 21:37
 */
public class HungrySingleton {
    private static HungrySingleton hungrySingleton = new HungrySingleton();
    private HungrySingleton(){
        if(null != hungrySingleton){
            throw new RuntimeException("反射攻击");
        }
    }
    public static HungrySingleton getInstance(){
        return hungrySingleton;
    }
}
