package com.cimu.creational.singleton;

/**
 * Title: MyRunnable
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月11日 19:39
 */
public class MyRunnable implements Runnable {
    public void run() {
//        LazySingleton lazySingleton = LazySingleton.getInstance();
//        System.out.println(lazySingleton);
        LazyThreadSingleton lazyThreadSingleton = LazyThreadSingleton.getInstance();
        System.out.println(lazyThreadSingleton);
    }
}
