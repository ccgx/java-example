package com.cimu.creational.builder.v1;

/**
 * Title: CarBuilderDirector
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月10日 12:14
 */
public class CarBuilderDirector {
    private CarBuilder carBuilder;
    public void setCarBuilder(CarBuilder carBuilder){
        this.carBuilder = carBuilder;
    }
    public Car makeCar(String color,int wheels){
        carBuilder.buildColor(color);
        carBuilder.buildWheels(wheels);
        return carBuilder.makeCar();
    }
}
