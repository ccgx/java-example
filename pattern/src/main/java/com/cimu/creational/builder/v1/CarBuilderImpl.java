package com.cimu.creational.builder.v1;

/**
 * Title: CarBuilderImpl
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月10日 12:12
 */
public class CarBuilderImpl extends CarBuilder {
    private Car car = new Car();
    @Override
    public void buildColor(String color) {
        car.setColor(color);
    }
    @Override
    public void buildWheels(int wheels) {
        car.setWheels(wheels);
    }
    @Override
    public Car makeCar() {
        return car;
    }
}
