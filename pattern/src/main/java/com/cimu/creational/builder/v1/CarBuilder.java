package com.cimu.creational.builder.v1;

/**
 * Title: CarBuilder
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月10日 12:10
 */
public abstract class CarBuilder {
    public abstract void buildColor(String color);
    public abstract void buildWheels(int wheels);
    public abstract Car makeCar();
}
