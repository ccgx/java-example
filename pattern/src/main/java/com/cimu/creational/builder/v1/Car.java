package com.cimu.creational.builder.v1;

/**
 * Title: Car
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月10日 12:08
 */
public class Car {
    private String color;
    private int wheels;
    public Car() {
    }
    public String getColor() {
        return color;
    }
    public void setColor(String color) {
        this.color = color;
    }
    public int getWheels() {
        return wheels;
    }
    public void setWheels(int wheels) {
        this.wheels = wheels;
    }
    @Override
    public String toString() {
        return "Car{" +
                "color='" + color + '\'' +
                ", wheels=" + wheels +
                '}';
    }
}
