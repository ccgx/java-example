package com.cimu.creational.builder.v2;

/**
 * Title: BuilderTest
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月10日 12:33
 */
public class BuilderTest {
    public static void main(String[] args) {
        Car car = new Car.CarBuilder().buildColor("红色").buildWheels(4).build();
        System.out.println(car);
    }
}
