package com.cimu.creational.builder.v1;

/**
 * Title: BuilderTest
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月10日 12:17
 */
public class BuilderTest {
    public static void main(String[] args) {
        CarBuilder carBuilder = new CarBuilderImpl();
        CarBuilderDirector carBuilderDirector = new CarBuilderDirector();
        carBuilderDirector.setCarBuilder(carBuilder);
        Car car = carBuilderDirector.makeCar("红色",4);
        System.out.println(car);
    }
}
