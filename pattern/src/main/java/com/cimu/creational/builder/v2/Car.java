package com.cimu.creational.builder.v2;

/**
 * Title: Car
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月10日 12:08
 */
public class Car {
    private String color;
    private int wheels;
    public Car(CarBuilder carBuilder) {
        this.color = carBuilder.color;
        this.wheels = carBuilder.wheels;
    }
    @Override
    public String toString() {
        return "Car{" +
                "color='" + color + '\'' +
                ", wheels=" + wheels +
                '}';
    }
    public static class CarBuilder{
        private String color;
        private int wheels;
        public CarBuilder buildColor(String color){
            this.color = color;
            return this;
        }
        public CarBuilder buildWheels(int wheels){
            this.wheels = wheels;
            return this;
        }
        public Car build(){
            return new Car(this);
        }
    }
}
