package com.cimu.creational.factorymethod;

/**
 * Title: IphoneMobileFactory
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月06日 17:08
 */
public class IphoneMobileFactory extends FoxconnFactory {
    @Override
    public Mobile getMobile() {
        return new IphoneMobile();
    }
}
