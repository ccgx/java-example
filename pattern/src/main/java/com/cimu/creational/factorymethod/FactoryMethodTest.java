package com.cimu.creational.factorymethod;

/**
 * Title: FactoryMethodTest
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月02日 12:15
 */
public class FactoryMethodTest {
    public static void main(String[] args) {
        FoxconnFactory iphoneMobileFactory = new IphoneMobileFactory();
        Mobile iphoneMobile = iphoneMobileFactory.getMobile();
        iphoneMobile.produce();
        FoxconnFactory huaweiMobileFactory = new HuaweiMobileFactory();
        Mobile huaweiMobile = huaweiMobileFactory.getMobile();
        huaweiMobile.produce();
    }
}
