package com.cimu.creational.factorymethod;

/**
 * Title: Mobile
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月02日 12:19
 */
public interface Mobile {
    void produce();
}
