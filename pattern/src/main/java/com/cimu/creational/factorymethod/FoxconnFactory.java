package com.cimu.creational.factorymethod;

/**
 * Title: FoxconnFactory
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月02日 12:20
 */
public abstract class FoxconnFactory {
    public abstract Mobile getMobile();
}
