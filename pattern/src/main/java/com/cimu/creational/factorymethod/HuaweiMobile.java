package com.cimu.creational.factorymethod;

/**
 * Title: HuaweiMobile
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月02日 12:22
 */
public class HuaweiMobile implements Mobile {
    @Override
    public void produce() {
        System.out.println("生产华为手机");
    }
}
