package com.cimu.creational.factorymethod;

/**
 * Title: HuaweiMobileFactory
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月06日 17:07
 */
public class HuaweiMobileFactory extends FoxconnFactory {
    @Override
    public Mobile getMobile() {
        return new HuaweiMobile();
    }
}
