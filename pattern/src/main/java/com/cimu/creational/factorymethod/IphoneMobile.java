package com.cimu.creational.factorymethod;

/**
 * Title: IphoneMobile
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年07月02日 12:21
 */
public class IphoneMobile implements Mobile {
    @Override
    public void produce() {
        System.out.println("生产苹果手机");
    }
}
