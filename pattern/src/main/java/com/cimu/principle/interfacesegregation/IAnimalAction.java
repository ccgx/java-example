package com.cimu.principle.interfacesegregation;

/**
 * Created by cgx
 */
public interface IAnimalAction {
    void eat();
    void fly();
    void swim();

}
