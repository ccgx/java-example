package com.cimu.principle.interfacesegregation;

/**
 * Created by cgx
 */
public interface ISwimAnimalAction {
    void swim();
}
