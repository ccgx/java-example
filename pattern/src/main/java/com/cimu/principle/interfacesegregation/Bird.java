package com.cimu.principle.interfacesegregation;

/**
 * Created by cgx
 */
public class Bird implements IAnimalAction {
    @Override
    public void eat() {

    }

    @Override
    public void fly() {

    }

    @Override
    public void swim() {

    }
}
