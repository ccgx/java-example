package com.cimu.principle.interfacesegregation;

/**
 * Created by cgx
 */
public interface IEatAnimalAction {
    void eat();
}
