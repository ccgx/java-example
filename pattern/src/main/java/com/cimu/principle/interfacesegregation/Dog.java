package com.cimu.principle.interfacesegregation;

/**
 * Created by cgx
 */
public class Dog implements ISwimAnimalAction,IEatAnimalAction {

    @Override
    public void eat() {

    }

    @Override
    public void swim() {

    }
}
