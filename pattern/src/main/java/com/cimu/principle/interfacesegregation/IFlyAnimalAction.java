package com.cimu.principle.interfacesegregation;

/**
 * Created by cgx
 */
public interface IFlyAnimalAction {
    void fly();
}
