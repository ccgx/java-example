package com.cimu.principle.liskovsubstitution.methodoutput;

/**
 * Created by cgx
 */
public class Test {
    public static void main(String[] args) {
        Child child = new Child();
        System.out.println(child.method());
    }
}
