package com.cimu.principle.liskovsubstitution;

/**
 * Created by cgx
 */
public interface Quadrangle {
    long getWidth();
    long getLength();

}
