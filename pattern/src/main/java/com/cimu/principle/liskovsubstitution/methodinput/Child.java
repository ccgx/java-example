package com.cimu.principle.liskovsubstitution.methodinput;

import java.util.Map;

/**
 * Created by cgx
 */
public class Child extends Base {
    public void method(Map map) {
        System.out.println("子类HashMap入参方法被执行");
    }
}
