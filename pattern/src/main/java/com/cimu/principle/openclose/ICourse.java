package com.cimu.principle.openclose;

/**
 * Created by cgx
 */
public interface ICourse {
    Integer getId();
    String getName();
    Double getPrice();


}
