package com.cimu.principle.singleresponsibility;

/**
 * Created by cgx
 */
public interface ICourseContent {
    String getCourseName();
    byte[] getCourseVideo();
}
