package com.cimu.principle.singleresponsibility.v1;


/**
 * Created by cgx
 */
public class CourseImpl implements ICourse {
    @Override
    public void studyCourse() {

    }

    @Override
    public void refundCourse() {

    }

    @Override
    public String getCourseName() {
        return null;
    }

    @Override
    public byte[] getCourseVideo() {
        return new byte[0];
    }
}
