package com.cimu.principle.singleresponsibility.v1;

/**
 * Created by cgx
 */
public interface ICourse {
    //课程内容的信息
    String getCourseName();
    byte[] getCourseVideo();
    //用户对课程的行为
    void studyCourse();
    void refundCourse();
}
