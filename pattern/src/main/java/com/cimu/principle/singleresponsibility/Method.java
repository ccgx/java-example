package com.cimu.principle.singleresponsibility;

/**
 * Created by cgx
 */
public class Method {
    private void updateUserInfo(String userName,String address){
        userName = "张三";
        address = "beijing";
    }

    private void updateUserInfo(String userName,String... properties){
        userName = "张三";
//        address = "beijing";
    }

    private void updateUsername(String userName){
        userName = "张三";
    }
    private void updateUserAddress(String address){
        address = "beijing";
    }

    private void updateUserInfo(String userName,String address,boolean bool){
        if(bool){
            //todo something1
        }else{
            //todo something2
        }


        userName = "张三";
        address = "beijing";
    }


}
