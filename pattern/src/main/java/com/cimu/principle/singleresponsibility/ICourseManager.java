package com.cimu.principle.singleresponsibility;

/**
 * Created by cgx
 */
public interface ICourseManager {
    void studyCourse();
    void refundCourse();
}
