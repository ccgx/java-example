package com.cimu.principle.dependenceinversion;

/**
 * Created by cgx
 */
public class PythonCourse implements ICourse {
    @Override
    public void studyCourse() {
        System.out.println("张三在学习Python课程");
    }
}
