package com.cimu.principle.dependenceinversion;

/**
 * Created by cgx
 */
public interface ICourse {
    void studyCourse();
}
