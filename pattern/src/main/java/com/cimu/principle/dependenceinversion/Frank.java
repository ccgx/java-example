package com.cimu.principle.dependenceinversion;

/**
 * Created by cgx
 */
public class Frank {

    public void setiCourse(ICourse iCourse) {
        this.iCourse = iCourse;
    }

    private ICourse iCourse;

    public void studyImoocCourse(){
        iCourse.studyCourse();
    }

}
