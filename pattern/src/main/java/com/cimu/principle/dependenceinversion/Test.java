package com.cimu.principle.dependenceinversion;

/**
 * Created by cgx
 */
public class Test {

    public static void main(String[] args) {
        Frank frank = new Frank();
        frank.setiCourse(new JavaCourse());
        frank.studyImoocCourse();

        frank.setiCourse(new FECourse());
        frank.studyImoocCourse();

    }


}
