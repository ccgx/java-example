package com.cimu.principle.compositionaggregation;

/**
 * Created by cgx
 */
public abstract class DBConnection {
    public abstract String getConnection();
}
