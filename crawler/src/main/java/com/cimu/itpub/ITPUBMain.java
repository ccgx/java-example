package com.cimu.itpub;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.cimu.utils.HttpRequest;

import java.io.File;
import java.io.FileWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Title: itpub
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年08月17日 13:00
 */
public class ITPUBMain {


    public static void main(String[] args) throws Exception {
        String menuUrl = "https://api.z.itpub.net/stack/menulist?stackid=10131&type=1";
        Map<String,String> headerMap = new HashMap<>();
        headerMap.put("content-type","application/x-www-form-urlencoded;charset=UTF-8");
        headerMap.put("st-usertoken", "faeb9ed134ea0edceccd6da38822f145");
        String result = HttpRequest.get(menuUrl, null, headerMap);
        JSONObject menuJsonObj = JSON.parseObject(result);
        String menuStr = String.valueOf(menuJsonObj.get("data"));
        List<JSONObject> menuAry = JSON.parseArray(menuStr,JSONObject.class);
        String articleListUrl = "https://api.z.itpub.net/stack/getmenudynamic?stackid=10131&menuid=";
        String articleDetailUrl = "https://api.z.itpub.net/article/detail?publish_userid=31556838&articleid=";
        for(JSONObject menu : menuAry){
            //菜单列表
            String menuName = String.valueOf(menu.get("name"));
            String menuId = menu.getString("id");
            System.out.println(menuId+"_"+menuName);
            if("143".equals(menuId) || "108".equals(menuId) ||"109".equals(menuId)|| "110".equals(menuId)|| "111".equals(menuId)|| "112".equals(menuId)|| "113".equals(menuId)|| "114".equals(menuId)){
                continue;
            }

            File menuFile = new File("E:/itpub/"+menuName);
            if(!menuFile.exists()){
                menuFile.mkdirs();
            }
            //获取文章列表
            String articleListResult = HttpRequest.get(articleListUrl+menu.getString("id"), null, headerMap);
            JSONObject articleListJsonObj = JSON.parseObject(articleListResult);
            String articleListStr = String.valueOf(articleListJsonObj.get("data"));
            JSONObject articleListDataObj = JSON.parseObject(articleListStr);
            String articleListListStr = String.valueOf(articleListDataObj.get("lists"));
            List<JSONObject> articleListAry = JSON.parseArray(articleListListStr,JSONObject.class);
            int i=0;
            for(JSONObject jsonObject: articleListAry){
                i++;
                String contentTitle = jsonObject.getString("content");
                String articleid = jsonObject.getString("articleid");
                String articleDetailResult = HttpRequest.get(articleDetailUrl+articleid, null, headerMap);
                JSONObject articleDetailResultJSONObj = JSON.parseObject(articleDetailResult);
                String articleData = articleDetailResultJSONObj.getString("data");
                List<JSONObject> articleDataObjAry = JSON.parseArray(articleData,JSONObject.class);
                JSONObject articleContent = JSON.parseObject(String.valueOf(articleDataObjAry.get(0)));
                File htmlFile = new File(menuFile.getPath()+"/"+
                        i+contentTitle.replaceAll("\\?","_").replaceAll("\\，","_").replaceAll("\\|","_").replaceAll("\\*","_").replaceAll("\\/","_").replaceAll("<","_").replaceAll(">","_")+".html");
                if(!htmlFile.exists()){
                    Writer out =new FileWriter(htmlFile);
                    out.write(articleContent.getString("content"));
                    out.close();
                    Thread.sleep(5000);
                }
            }
        }

    }

}
