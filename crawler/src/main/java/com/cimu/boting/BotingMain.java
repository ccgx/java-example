package com.cimu.boting;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;

/**
 * Title: BotingMain
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年08月17日 13:34
 */
public class BotingMain {
    public static void main(String[] args) {
        try {
            String url = "http://www.boting.co/book/11941-374150.htm";
            //先获得的是整个页面的html标签页面
            Document doc = Jsoup.connect(url).get();
            //audio id=jp_audio_0 src=资源
            //获取正文标题，因为整片文章只有标题是用h1标签
            Element btEl = doc.selectFirst("audio[id=jp_audio_0]");
            String  bt=btEl.attr("src");
            System.out.println(bt);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
