package com.cimu.utils;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.TimeUnit;


public class HttpRequest {

    private static OkHttpClient client;

    private static OkHttpClient getClient() {
        if (client == null) {

            client = new OkHttpClient.Builder()
                    .connectTimeout(10, TimeUnit.SECONDS)
                    .readTimeout(20, TimeUnit.SECONDS)
                    .writeTimeout(20, TimeUnit.SECONDS)
                    .retryOnConnectionFailure(true)
                    .build();
        }
        return client;
    }


    public static String get(String url, Map<String, String> parameters, Map<String, String> headers) {
        String params = "";
        if (null != parameters) {
            for (Map.Entry<String, String> entry : parameters.entrySet()) {
                params += entry.getKey() + "=" + entry.getValue() + "&";
            }
            params = params.substring(0, params.length() - 1);
        }
        if (url.endsWith("/")) {
            url = url.substring(0, url.length() - 1);
        }
        if (null != parameters) {
            url = url + "?" + params;
        }
        Request.Builder builder = new Request.Builder()
                .addHeader("Connection", "close")
                .url(url);
        if (headers != null) {
            for (Map.Entry<String, String> entry : headers.entrySet()) {
                builder.addHeader(entry.getKey(), entry.getValue());
            }
        }
        Request request = builder.build();


        try (Response response = getClient().newCall(request).execute()) {
            return response.body().string();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 模拟表单的提交
     *
     * @param url
     * @param param
     */
    public static String post(String url, Map<String, String> param, Map<String, String> headers) {

        FormBody.Builder form_builder = new FormBody.Builder();  //表单对象，包含以input开始的对象，模拟一个表单操作，以HTML表单为主
        //如果键值对不为空，且值不为空
        if (param != null && !param.isEmpty()) {
            //循环这个表单，zengqiang for循环
            for (Map.Entry<String, String> entry : param.entrySet()) {
                form_builder.add(entry.getKey(), entry.getValue());
            }
        }
        //声明一个请求对象体
        RequestBody request_body = form_builder.build();

        Request.Builder builder = new Request.Builder()
                .addHeader("Connection", "close")
                .url(url).post(request_body);
        if (headers != null) {
            for (Map.Entry<String, String> entry : headers.entrySet()) {
                builder.addHeader(entry.getKey(), entry.getValue());
            }
        }
        //采用post的方式进行提交
        Request request = builder.build();
        try (Response response = getClient().newCall(request).execute()) {
            return response.body().string();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}