package java8.defaultmethod3;

/**
 * Title: MyClass
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年06月22日 15:15
 */
public class MyClass extends MyInterfaceImpl implements  MyInterface2 {
    public static void main(String[] args) {
        MyClass myClass = new MyClass();
        myClass.test();
    }
}
