package java8.defaultmethod3;

/**
 * Title: MyInterface2
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年06月22日 15:15
 */
public interface MyInterface2 {
    default void test(){
        System.out.println("test2");
    }
}
