package java8.defaultmethod3;

/**
 * Title: MyInterfaceImpl
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年06月22日 15:25
 */
public class MyInterfaceImpl implements MyInterface1{
    @Override
    public void test(){
        System.out.println("test impl");
    }
}
