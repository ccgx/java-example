package java8.functionalinterface;

/**
 * Title: FunctionalClass01
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年06月23日 13:34
 */
public class FunctionalClass02 {
    public void myTest(FunctionalInterface01 functionalInterface01){
        System.out.println(1);
        functionalInterface01.test();
        System.out.println(2);
    }
    public static void main(String[] args) {
        FunctionalClass02 class01 = new FunctionalClass02();
        class01.myTest(() -> System.out.println("test"));


        FunctionalInterface01 functionalInterface01 = ()->System.out.println("test");

    }
}
