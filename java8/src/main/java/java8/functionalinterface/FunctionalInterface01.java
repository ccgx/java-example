package java8.functionalinterface;

/**
 * Title: FunctionalInterface01
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年06月23日 10:41
 */
@FunctionalInterface
public interface FunctionalInterface01 {
    //抽象方法
    void test();
    //默认方法
    default void test01(){
    }
    //默认方法
    default void test02(){
    }
    //是java.lang.Object的方法
    @Override
    String toString();
}

