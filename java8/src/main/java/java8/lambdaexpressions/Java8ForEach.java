package java8.lambdaexpressions;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.List;

/**
 * Title: Java8ForEach
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年06月22日 16:30
 */
public class Java8ForEach {
    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(1,2,3,4,5,6,7,8);
        for(int i=0;i<list.size();i++){
            System.out.println(list.get(i));
        }

        for(Integer i : list){
            System.out.println(i);
        }
        //java8 之后
        list.forEach(i->System.out.println(i));
        list.forEach(System.out::println);
    }
}
