package java8.lambdaexpressions;

/**
 * Title: Car
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年06月23日 17:23
 */
public class User {
    private String name;
    private String age;

    public User() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }
}
