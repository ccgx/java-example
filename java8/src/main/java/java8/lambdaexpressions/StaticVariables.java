package java8.lambdaexpressions;

/**
 * Title: StaticVariables
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年06月23日 18:41
 */
public class StaticVariables {
    static int outerStaticNum;
    int outerNum;
    void testScopes() {
        Converter<Integer, String> stringConverter1 = (from) -> {
            outerNum = 23;
            return String.valueOf(from);
        };
        Converter<Integer, String> stringConverter2 = (from) -> {
            outerStaticNum = 72;
            return String.valueOf(from);
        };
    }
}
