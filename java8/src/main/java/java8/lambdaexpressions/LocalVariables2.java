//package java8.lambdaexpressions;
//
///**
// * Title: LocalVariables
// * Copyright: Copyright (c) 2017
// *
// * @author cgx
// * date 2019年06月23日 18:35
// */
//public class LocalVariables2 {
//    public static void main(String[] args) {
//        int num = 1;
//        Converter<Integer, String> stringConverter =
//                (from) -> String.valueOf(from + num);
//        num = 3;
//    }
//}
