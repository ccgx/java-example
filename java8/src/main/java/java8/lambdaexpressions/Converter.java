package java8.lambdaexpressions;

/**
 * Title: Converter
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年06月23日 18:36
 */
@FunctionalInterface
public interface Converter<F, T> {
    T convert(F from);
}