package java8.lambdaexpressions;

import java.util.Arrays;
import java.util.List;

/**
 * Title: MethodReferences2
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年06月23日 17:22
 */
public class MethodReferences2 {
    private String name;
    private int age;
    public MethodReferences2(String name, int age) {
        this.name = name;
        this.age = age;
    }
    public int compareByAge(MethodReferences2 methodReferences2){
        return this.age- methodReferences2.age;
    }
    public static void main(String[] args) {
        MethodReferences2 constructorReferences1 = new MethodReferences2("zhangsan",20);
        MethodReferences2 methodReferences2 = new MethodReferences2("lisi",22);
        List<MethodReferences2> nameList = Arrays.asList(constructorReferences1, methodReferences2);
        nameList.sort(MethodReferences2::compareByAge);
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public int getAge() {
        return age;
    }
    public void setAge(int age) {
        this.age = age;
    }
}
