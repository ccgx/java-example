package java8.lambdaexpressions;

import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;

/**
 * Title: MethodReferences3
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年06月23日 17:22
 */
public class MethodReferences4 {
    public String getString(Supplier<String> supplier){
        return supplier.get()+"test";
    }
    public static void main(String[] args) {
        MethodReferences4 methodReferences4 = new MethodReferences4();
        System.out.println(methodReferences4.getString(String::new));
    }
}
