package java8.lambdaexpressions;

import java.util.Arrays;
import java.util.List;

/**
 * Title: MethodReferences3
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年06月23日 17:22
 */
public class MethodReferences3 {
    private String name;
    private int age;
    public MethodReferences3(String name, int age) {
        this.name = name;
        this.age = age;
    }
    public int compareByAge(MethodReferences3 constructorReferences2){
        return this.age-constructorReferences2.age;
    }
    public static void main(String[] args) {
        MethodReferences3 constructorReferences1 = new MethodReferences3("zhangsan",20);
        MethodReferences3 constructorReferences2 = new MethodReferences3("lisi",22);
        List<MethodReferences3> nameList = Arrays.asList(constructorReferences1,constructorReferences2);
        nameList.sort(MethodReferences3::compareByAge);
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public int getAge() {
        return age;
    }
    public void setAge(int age) {
        this.age = age;
    }
}
