package java8.lambdaexpressions;

import java.util.Arrays;
import java.util.List;

/**
 * Title: MethodReferences
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年06月23日 17:22
 */
public class MethodReferences {
    public static String myString(String str){
        System.out.println(str);
        return str;
    }
    public static void main(String[] args) {
        List<String> nameList = Arrays.asList("zhangsan","lisi");
        nameList.forEach(MethodReferences::myString);
    }
}
