package java8.lambdaexpressions;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Title: Jdk8Sort
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年06月22日 16:02
 */
public class Jdk8Sort {
    public static void main(String[] args) {
        List<String> names = Arrays.asList("zhangsan", "lisi", "wangwu", "zhaoliu");
        Collections.sort(names, (String a, String b) -> {
            return b.compareTo(a);
        });
        Collections.sort(names, (String a, String b) -> b.compareTo(a));
        Collections.sort(names, (a, b) -> b.compareTo(a));
        names.sort((a, b) -> b.compareTo(a));
        names.sort(Comparator.reverseOrder());
    }
}
