package java8.defaultmethod;

/**
 * Title: UserServiceMain
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年06月22日 14:42
 */
public class UserServiceMain {
    public static void main(String[] args) {
        UserService userService = new UserService(){
            @Override
            public double calculate(int a) {
                return sqrt(a);
            }
        };
        System.out.println(userService.calculate(100));//10
        System.out.println(userService.sqrt(16));//4
    }
}
