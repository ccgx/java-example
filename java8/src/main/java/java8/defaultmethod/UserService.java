package java8.defaultmethod;

/**
 * Title: UserService
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年06月22日 14:29
 */
public interface UserService {
    double calculate(int a);
    default double sqrt(int a){
        return Math.sqrt(a);
    }
}
