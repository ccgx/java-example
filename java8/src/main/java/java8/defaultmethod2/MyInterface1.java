package java8.defaultmethod2;

/**
 * Title: MyInterface1
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年06月22日 15:15
 */
public interface MyInterface1 {
    default void test(){
        System.out.println("test1");
    }
}
