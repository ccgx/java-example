package java8.defaultmethod2;

/**
 * Title: MyClass
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年06月22日 15:15
 */
public class MyClass implements MyInterface1,MyInterface2{
    @Override
    public void test() {
        MyInterface1.super.test();
    }
}
