package list;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Title: ObjectListToFieldList
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年05月10日 12:18
 */
public class ObjectListToFieldList {

    public static void main(String[] args) {
        List<User> userList = new ArrayList<>();
//        User user1 = new User();
//        user1.setId(1);
//        user1.setName("1");
//        userList.add(user1);
//        User user2 = new User();
//        user2.setId(2);
//        user2.setName("2");
//        userList.add(user2);
        for(int i=0;i<1000000;i++){
            User user1 = new User();
            user1.setId(i);
            user1.setName("name"+i);
            userList.add(user1);
        }
        java7(userList);
        java8(userList);
    }

    private static void java7(List<User> userList){
        long startTime = System.currentTimeMillis();
        List<String> nameList = new ArrayList<>();
        for(User user : userList){
            nameList.add(user.getName());
        }
        long endTime = System.currentTimeMillis();
        System.out.println(endTime-startTime);
    }

    private static void java8(List<User> userList){
        long startTime = System.currentTimeMillis();
        List<String> nameList = userList.stream().map(User::getName).collect(Collectors.toList());
        long endTime = System.currentTimeMillis();
        System.out.println(endTime-startTime);
    }

}
