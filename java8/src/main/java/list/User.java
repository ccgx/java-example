package list;

/**
 * Title: User
 * Copyright: Copyright (c) 2017
 *
 * @author cgx
 * date 2019年05月10日 12:19
 */
public class User {

    private Integer id;
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
