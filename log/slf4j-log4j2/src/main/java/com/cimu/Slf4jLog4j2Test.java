package com.cimu;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Title: Slf4jLog4j2Test
 * Copyright: Copyright (c) 2017
 * <p>
 * date 2019年02月18日 11:10
 */
public class Slf4jLog4j2Test {
    private static final Logger logger = LoggerFactory.getLogger(Slf4jLog4j2Test.class);

    public static void main(String[] args) {
        logger.info("log4j2");
    }
}
