package com.cimu;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Title: Slf4jJulTest
 * Copyright: Copyright (c) 2017
 * <p>
 * date 2019年02月18日 11:10
 */
public class Slf4jJulTest {
    private static Logger logger = LoggerFactory.getLogger(Slf4jJulTest.class);

    public static void main(String[] args) {
        logger.info("jul");
    }

}
