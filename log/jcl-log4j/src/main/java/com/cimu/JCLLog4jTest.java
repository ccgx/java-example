package com.cimu;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Title: JCLLog4jTest
 * Copyright: Copyright (c) 2017
 * <p>
 * date 2019年02月18日 11:10
 */
public class JCLLog4jTest {
    private static final Log logger = LogFactory.getLog(JCLLog4jTest.class);

    public static void main(String[] args) {
        logger.info("jcl-log4j");
    }
}
