package com.cimu;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Title: Slf4jMultiBridgeTest
 * Copyright: Copyright (c) 2017
 * <p>
 * date 2019年02月18日 10:35
 */
public class Slf4jMultiBridgeTest {
    private static Logger logger = LoggerFactory.getLogger(Slf4jMultiBridgeTest.class);

    public static void main(String[] args) {
        logger.info("test");
    }

}
