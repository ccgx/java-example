package com.cimu;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Title: LogbackTest
 * Copyright: Copyright (c) 2017
 * <p>
 * date 2019年02月18日 10:35
 */
public class LogbackTest {
    private static Logger logger = LoggerFactory.getLogger(LogbackTest.class);

    public static void main(String[] args) {
        logger.info("test");
    }

}
