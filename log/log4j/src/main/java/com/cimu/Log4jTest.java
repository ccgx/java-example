package com.cimu;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * Title: Log4jTest
 * Copyright: Copyright (c) 2017
 * <p>
 * date 2019年02月18日 11:10
 */
public class Log4jTest {
    private static Logger logger = LogManager.getLogger(Log4jTest.class);

    public static void main(String[] args) {
        logger.info("log4j");
    }
}
