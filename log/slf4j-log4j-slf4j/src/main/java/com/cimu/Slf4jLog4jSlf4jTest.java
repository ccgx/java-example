package com.cimu;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Title: Slf4jLog4jSlf4jTest
 * Copyright: Copyright (c) 2017
 * <p>
 * date 2019年02月18日 10:35
 */
public class Slf4jLog4jSlf4jTest {
    private static Logger logger = LoggerFactory.getLogger(Slf4jLog4jSlf4jTest.class);

    public static void main(String[] args) {
        logger.info("log4j");
    }

}
