package com.cimu.rocketmq.logappender;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

/**
 * log4j 生产者
 *
 */
public class Log4jProducer {
    private static Logger logger = Logger.getLogger(Log4jProducer.class);

    public static void main(String[] args) {
        PropertyConfigurator.configure("E:\\workspace\\java-example\\rocketmq-sample\\src\\main\\resources\\log4j.properties");
        logger.info("test");
    }

}
