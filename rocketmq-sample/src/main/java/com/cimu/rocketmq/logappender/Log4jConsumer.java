package com.cimu.rocketmq.logappender;

import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyStatus;
import org.apache.rocketmq.client.consumer.listener.MessageListenerConcurrently;

/**
 * log4j 消费者
 *
 * @author cgx on 2020/1/16
 */
public class Log4jConsumer {

    public static void main(String[] args) throws Exception {
        //初始化指定消费者的group名称
        DefaultMQPushConsumer consumer = new DefaultMQPushConsumer("logappender_consumer_name");

        //指定name server地址
        consumer.setNamesrvAddr("127.0.0.1:9876");

        // Subscribe one more more topics to consume.
        consumer.subscribe("log4jTopic", "*");
        // Register callback to execute on arrival of messages fetched from brokers.
        consumer.registerMessageListener((MessageListenerConcurrently) (msgs, context) -> {
            System.out.printf("%s Receive New Messages: %s %n", Thread.currentThread().getName(), new String(msgs.get(0).getBody()));
            return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
        });

        //Launch the consumer instance.
        consumer.start();

        System.out.printf("Consumer Started.%n");
    }
}
