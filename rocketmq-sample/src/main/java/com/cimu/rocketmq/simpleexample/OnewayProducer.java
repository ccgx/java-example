package com.cimu.rocketmq.simpleexample;

import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.remoting.common.RemotingHelper;

/**
 * 单向消息发送
 *
 * @author cgx on 2020/1/16
 */
public class OnewayProducer {
    public static void main(String[] args) throws Exception{
        //初始化生产者group名称
        DefaultMQProducer producer = new DefaultMQProducer("simple_producer_name");
        //指定nameserver地址
        producer.setNamesrvAddr("localhost:9876");
        //启动实例
        producer.start();
        for (int i = 0; i < 100; i++) {
            //创建消息实例，指定主题、标签、消息体
            Message msg = new Message("TopicTest" /* 主题 */,
                    "TagA" /* Tag */,
                    ("Hello RocketMQ " +
                            i).getBytes(RemotingHelper.DEFAULT_CHARSET) /* 消息内容 */
            );
            //调用发送消息方法进行消息发送
            producer.sendOneway(msg);

        }
        //生产者发送完之后需要给他关闭掉
        producer.shutdown();
    }
}
