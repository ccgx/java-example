package com.cimu.rocketmq.simpleexample;

import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.remoting.common.RemotingHelper;

/**
 * 同步消息生产者
 *
 * @author cgx on 2020/1/16
 */
public class SyncProducer {
    public static void main(String[] args) throws Exception {
        //初始化生产者group名称
        DefaultMQProducer producer = new
                DefaultMQProducer("simple_producer_name");
        //指定nameserver地址
        producer.setNamesrvAddr("localhost:9876");
        //启动实例
        producer.start();
        for (int i = 0; i < 100; i++) {
            //创建消息实例，指定主题、标签、消息体
            Message msg = new Message("TopicTest" /* 主题 */,
                    "TagA" /* Tag */,
                    ("Hello RocketMQ " +
                            i).getBytes(RemotingHelper.DEFAULT_CHARSET) /* 消息内容 */
            );
            //调用发送消息方法进行消息发送
            SendResult sendResult = producer.send(msg);
            System.out.printf("%s%n", sendResult);
        }
        //生产者发送完之后需要给他关闭掉
        producer.shutdown();
    }
}
