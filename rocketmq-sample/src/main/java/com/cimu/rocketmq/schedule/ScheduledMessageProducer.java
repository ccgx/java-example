package com.cimu.rocketmq.schedule;

import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.common.message.Message;

/**
 * 生产者
 *
 * @author cgx on 2020/1/16
 */
public class ScheduledMessageProducer {
    public static void main(String[] args) throws Exception {
        // 初始化生产者
        DefaultMQProducer producer = new DefaultMQProducer("scheduled_producer_name");
        producer.setNamesrvAddr("127.0.0.1:9876");
        // 启动实例
        producer.start();
        int totalMessagesToSend = 100;
        for (int i = 0; i < totalMessagesToSend; i++) {
            Message message = new Message("TopicScheduled", ("Hello scheduled message " + i).getBytes());
            //这个消息将延迟10秒消费
            message.setDelayTimeLevel(3);
            //发送消息
            producer.send(message);
        }

        // 关闭生产者
        producer.shutdown();
    }
}
