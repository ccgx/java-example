package com.cimu.rocketmq.batch;

import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.common.message.Message;

import java.util.ArrayList;
import java.util.List;

/**
 * 生产者
 *
 * @author cgx on 2020/1/16
 */
public class BatchProducerTwo {
    public static void main(String[] args) throws Exception {
        DefaultMQProducer producer = new DefaultMQProducer("batch_producer_name");
        producer.setNamesrvAddr("127.0.0.1:9876");
        producer.start();

        //大批消息
        String topic = "TopicBatch";
        List<Message> messages = new ArrayList<>(100 * 1000);
        for (int i = 0; i < 3 * 1000; i++) {
            messages.add(new Message(topic, "Tag", "OrderID" + i, ("Hello world " + i).getBytes()));
        }

        //将大批消息分成小批:
        ListSplitter splitter = new ListSplitter(messages);
        while (splitter.hasNext()) {
            List<Message> listItem = splitter.next();
            producer.send(listItem);
        }

        producer.shutdown();
    }
}
