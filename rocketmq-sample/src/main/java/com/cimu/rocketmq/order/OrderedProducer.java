package com.cimu.rocketmq.order;

import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.remoting.common.RemotingHelper;

/**
 * 顺序消息
 *
 * @author cgx on 2020/1/16
 */
public class OrderedProducer {

    public static void main(String[] args) throws Exception {
        //初始化生产者group名称
        DefaultMQProducer producer = new DefaultMQProducer("order_producer_name");
        producer.setNamesrvAddr("127.0.0.1:9876");
        //启动实例
        producer.start();
        for (int i = 0; i < 10; i++) {
            //创建消息实例，指定主题、标签、消息体
            Message msg = new Message("TopicOrderTest", "tagA", "KEY" + i,
                    ("Hello RocketMQ " + i).getBytes(RemotingHelper.DEFAULT_CHARSET));
            SendResult sendResult = producer.send(msg, (mqs, msg1, arg) -> {
                Integer id = (Integer) arg;
                int index = id % mqs.size();
                System.out.println("producer:queueId:"+id+";content:"+new String(msg.getBody()));
                return mqs.get(index);
            }, 3);

        }
        //生产者关闭
        producer.shutdown();
    }
}
