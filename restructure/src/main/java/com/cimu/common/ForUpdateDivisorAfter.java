package com.cimu.common;

/**
 * 在for循环内修正增量因子有什么弊端
 */
public class ForUpdateDivisorAfter {

    public static void method(int count){
        for (int i = 0; i < 10;i+=1) {
            System.out.println(i);
        }
    }

}

