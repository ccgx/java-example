package com.cimu.common;

import java.util.ArrayList;
import java.util.List;

/**
 * 如何正确使用通配符的边界
 */
public class UseWildcardAfter {

    public void method() {
        List<Number> list = new ArrayList<>();
        write(list);
    }

    private void write(List<? super Number> list) {
        list.add(1.1);
        list.add(2);
        System.out.println(list);
    }

}

