package com.cimu.common;

import java.util.ArrayList;
import java.util.List;

/**
 * 要习惯于用泛型代替原生类型
 */
public class UseGenericAfter<T> {

    private T object;

    public UseGenericAfter(T object) {
        this.object = object;
    }

    public void showType() {
        System.out.println("实际的类型为：" + object.getClass().getName());
    }

    public T getObject() {
        return object;
    }

    public void setObject(T object) {
        this.object = object;
    }

    public static void main(String[] args) {
        UseGenericAfter<Integer> useGenericBefore = new UseGenericAfter<>(new Integer(1));
        useGenericBefore.showType();
        int int1 = useGenericBefore.getObject();
        System.out.println("value1=" + int1);
        UseGenericAfter<String> useGenericBefore2 = new UseGenericAfter<>("");
        useGenericBefore.showType();
        String str = useGenericBefore2.getObject();
        System.out.println("value2=" + int1);
        List<Integer> list = new ArrayList<>();
        list.add(1);
        System.out.println(list.get(1));
    }

}

