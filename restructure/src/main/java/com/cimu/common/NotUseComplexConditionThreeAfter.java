package com.cimu.common;

/**
 * 避免使用复杂条件式或分支3--用多态代替条件表达式
 */
public class NotUseComplexConditionThreeAfter {

    public void method(){
        Fruits apple = new Apple("苹果");
        apple.eatMethod();

        Fruits banana = new Banana("香蕉");
        banana.eatMethod();
    }

}

class Banana extends Fruits {

    public Banana(String name) {
        this.name = name;
    }

    @Override
    public void eatMethod() {
        System.out.println(this.name + "剥皮吃");
    }
}


class Apple extends Fruits {

    public Apple(String name) {
        this.name = name;
    }

    @Override
    public void eatMethod() {
        System.out.println(this.name + "削皮吃");
    }
}

abstract class Fruits {

    //水果名称
    protected String name;

    //水果吃法
    public void eatMethod() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}