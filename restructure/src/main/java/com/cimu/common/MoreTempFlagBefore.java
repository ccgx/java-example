package com.cimu.common;

/**
 * 避免赋予临时变量过多的角色
 */
public class MoreTempFlagBefore {

    public static void method() {
        String temp;
        temp = "it is " + getType();
        System.out.println(temp);
        temp = "this is " + getName();
        System.out.println(temp);
    }

    private static String getName() {
        return "张三";
    }

    private static String getType() {
        return "aaaa";
    }

}

