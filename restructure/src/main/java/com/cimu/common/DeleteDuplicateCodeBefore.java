package com.cimu.common;

/**
 * 去掉重复代码
 */
public class DeleteDuplicateCodeBefore {

   public static void method(){
        printName("张三");
        printDepart("研发中心");
   }

    public static void printName(String name){
        System.out.println(name);
    }
    public static void printDepart(String depart){
        System.out.println(depart);
    }
}

