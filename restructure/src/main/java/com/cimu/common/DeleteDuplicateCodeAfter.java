package com.cimu.common;

/**
 * 去掉重复代码
 */
public class DeleteDuplicateCodeAfter {

    public static void method(){
        printContent("张三");
        printContent("研发中心");
    }

    public static void printContent(String content){
        System.out.println(content);
    }
}

