package com.cimu.common;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;

/**
 * 用BigDecimal类型进行精确计算
 */
public class UseBigDecimalAfter {

    public static void method(int count){
        BigDecimal b1 = new BigDecimal("99.00");
        BigDecimal b2 = new BigDecimal("88.90");
        NumberFormat nf = new DecimalFormat("#.##");
        System.out.println(nf.format(b1.subtract(b2)));
    }

}

