package com.cimu.common;

/**
 * 用Enum代替Integer类型码常量
 */
public class UseEnumAfter {

    public static String getDayName(WeekEnum type) {
        if (WeekEnum.MONDAY == type) {
            return "周一";
        } else if (WeekEnum.TUESDAY == type) {
            return "周二";
        } else if (WeekEnum.WEDNESDAY == type) {
            return "周三";
        } else if (WeekEnum.THURSDAY == type) {
            return "周四";
        } else if (WeekEnum.FRIDAY == type) {
            return "周五";
        } else if (WeekEnum.SATURDAY == type) {
            return "周六";
        } else {
            return "周日";
        }
    }

}

enum WeekEnum{
    SUNDAY,MONDAY,TUESDAY,WEDNESDAY,THURSDAY,FRIDAY ,SATURDAY
}
