package com.cimu.common;

/**
 * 避免混用复杂运算符
 */
public class NotUseComplexCalcAfter {

    public static void method(int times) {
        int totalScore = 20 + getScore() * times;
        System.out.println(totalScore > 100);
    }

    private static int getScore() {
        return 10;
    }

}

