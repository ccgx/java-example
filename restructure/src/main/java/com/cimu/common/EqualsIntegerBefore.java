package com.cimu.common;

/**
 * 如何深入理解“==”的真正含义
 */
public class EqualsIntegerBefore {

    public static void method() {
        Integer num1 = 22000;
        Integer num2 = 23000;
        System.out.println(num1 == num2);
    }

}

