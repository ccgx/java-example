package com.cimu.common;

/**
 * 避免使用复杂条件式或分支3
 */
public class NotUseComplexConditionThreeBefore {

    public static String eatMethod(Integer type) {
        //苹果吃法
        if (NotUseConsts.FRUITS_APPLE.equals(type)) {
            return "苹果削皮吃";
            //香蕉吃法
        } else if (NotUseConsts.FRUITS_BANANA.equals(type)) {
            return "香蕉剥皮吃";
        }
        return "";
    }

}

class NotUseConsts {

    public static final Integer FRUITS_APPLE = 0;

    public static final Integer FRUITS_BANANA = 1;

}