package com.cimu.common;

/**
 * 避免使用复杂条件式或分支1
 */
public class NotUseComplexConditionOneBefore {

    public static void method() {
        //是否幸福 月入大于2000
        if (getBaseMoney("1") + getBonus(205) - getTax("") - 1000 > 2000) {
            System.out.println("开心");
        }
    }

    private static int getTax(String type) {
        if ("1".equals(type)) {
            return 200;
        }
        return 300;
    }

    private static int getBonus(int workTime) {
        if (workTime > 200) {
            return 1000;
        }
        return 0;
    }

    private static int getBaseMoney(String type) {
        if ("1".equals(type)) {
            return 2000;
        }
        return 4000;
    }

}

