package com.cimu.common;

import java.util.ArrayList;
import java.util.List;

/**
 * 要习惯于用泛型代替原生类型
 */
public class UseGenericBefore {

    private Object object;

    public UseGenericBefore(Object object) {
        this.object = object;
    }

    public void showType() {
        System.out.println("实际的类型为：" + object.getClass().getName());
    }

    public Object getObject() {
        return object;
    }

    public void setObject(Object object) {
        this.object = object;
    }

    public static void main(String[] args) {
        UseGenericBefore useGenericBefore = new UseGenericBefore(new Integer(1));
        useGenericBefore.showType();
        int int1 = (Integer) useGenericBefore.getObject();
        System.out.println("value1=" + int1);
        UseGenericBefore useGenericBefore2 = new UseGenericBefore("");
        useGenericBefore.showType();
        String str = (String) useGenericBefore2.getObject();
        System.out.println("value2=" + int1);
        List list = new ArrayList();
        list.add(1);
        list.add("哈哈");
        System.out.println((Integer) list.get(1));
    }

}

