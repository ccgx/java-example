package com.cimu.common;

/**
 * 避免在一条语句中声明或赋值多个变量
 */
public class DefinitionMoreVariableBefore {

    public static void method(int count){
        long long1,long2;
        long1 = long2 = 1L;
        System.out.println(long1);
        System.out.println(long2);
    }

}

