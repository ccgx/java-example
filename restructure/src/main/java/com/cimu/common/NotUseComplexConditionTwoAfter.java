package com.cimu.common;

/**
 * 避免使用复杂条件式或分支2--使用卫语句
 */
public class NotUseComplexConditionTwoAfter {

    public static int getInsurance(String type) {
        if ("1".equals(type)) {
            //死亡
            return 1000000;
        }
        //失业
        if ("2".equals(type)) {
            return 50000;
        }
        if ("3".equals(type)) {
            return 10000;
        }
        return 0;
    }

}

