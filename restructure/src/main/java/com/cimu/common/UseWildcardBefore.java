package com.cimu.common;

import java.util.ArrayList;
import java.util.List;

/**
 * 如何正确使用通配符的边界
 */
public class UseWildcardBefore {

    public void method(){
        List<Double> list = new ArrayList<>();
        list.add(1.1);
        read(list);
        List<Integer> list2 = new ArrayList<>();
        list2.add(2);
        read(list2);
    }

    private void read(List<? extends Number> list) {
        for(Object o : list){
            System.out.println(o);
        }
    }

}

