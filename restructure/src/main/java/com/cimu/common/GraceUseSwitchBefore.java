package com.cimu.common;

/**
 * 优雅的使用switch
 */
public class GraceUseSwitchBefore {

    public static void method(int count){
        switch (count){
            case 0 :
                System.out.println("0");
            case 1:
                System.out.println("1");
                break;
            case 2:
                System.out.println("2");
                break;
        }
    }

}

