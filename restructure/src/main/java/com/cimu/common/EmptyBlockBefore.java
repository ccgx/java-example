package com.cimu.common;

import java.io.IOException;

/**
 * 避免使用空块
 */
public class EmptyBlockBefore {

    public static void method(boolean isTrue) {
        if (isTrue) ;
        System.out.println("before");
    }

    public static void method2(boolean para) {
        boolean isTrue = para;
        {

        }
        if (isTrue) {
        }
        System.out.println("before");
    }

    public static void method3() {
        try {
            int in = System.in.read();
            System.out.println(in);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
        }
    }

}
