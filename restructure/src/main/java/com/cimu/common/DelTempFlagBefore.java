package com.cimu.common;

/**
 * 去掉控制标志的临时变量
 */
public class DelTempFlagBefore {

    public static void method() {
        System.out.println(getLevel(1));
    }

    public static String getLevel(int type) {
        String resultData = "";
        if (type == 1) {
            resultData = "第一";
        } else if (type == 2) {
            resultData = "第二";
        } else {
            resultData = "第三";
        }
        return resultData;
    }

}

