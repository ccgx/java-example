package com.cimu.common;

/**
 * 避免混用复杂运算符
 */
public class NotUseComplexCalcBefore {

    public static void method(int times) {
        System.out.println(20 + getScore() * times > 100);
    }

    private static int getScore() {
        return 10;
    }

}

