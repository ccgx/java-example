package com.cimu.common;

/**
 * 避免使用复杂条件式或分支2
 */
public class NotUseComplexConditionTwoBefore {

    public static int getInsurance(String type) {
        int result = 0;
        if ("1".equals(type)) {
            //死亡
            result = 1000000;
        } else {
            //失业
            if ("2".equals(type)) {
                result = 50000;
            } else {
                if ("3".equals(type)) {
                    result = 10000;
                }
            }
        }
        return result;
    }


}

