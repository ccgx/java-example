package com.cimu.common;

/**
 * 去掉控制标志的临时变量
 */
public class DelTempFlagAfter {

    public static void method() {
        System.out.println(getLevel(1));
    }

    public static String getLevel(int type) {
        String resultData = "";
        if (type == 1) {
            return "第一";
        } else if (type == 2) {
            return "第二";
        } else {
            return "第三";
        }
    }


}

