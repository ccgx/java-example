package com.cimu.common;

/**
 * 用大写L代替小写l定义long变量
 */
public class UseUpperLongBefore {

    public static void method(int count){
        long l=1l;
        System.out.println(l);
    }

}

