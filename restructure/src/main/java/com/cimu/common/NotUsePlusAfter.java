package com.cimu.common;

/**
 * 避免混用“+”
 */
public class NotUsePlusAfter {

    public static void method(int count){
        int result = 2+4;
        System.out.println("2+4="+result);
    }

}

