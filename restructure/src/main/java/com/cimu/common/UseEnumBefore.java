package com.cimu.common;

/**
 * 用Enum代替Integer类型码常量
 */
public class UseEnumBefore {

    public static String getDayName(int type) {
        if (WeekConsts.WEEK_MONDAY == type) {
            return "周一";
        } else if (WeekConsts.WEEK_TUESDAY == type) {
            return "周二";
        } else if (WeekConsts.WEEK_WEDNESDAY == type) {
            return "周三";
        } else if (WeekConsts.WEEK_THURSDAY == type) {
            return "周四";
        } else if (WeekConsts.WEEK_FRIDAY == type) {
            return "周五";
        } else if (WeekConsts.WEEK_SATURDAY == type) {
            return "周六";
        } else {
            return "周日";
        }
    }

}

class WeekConsts {

    public static final Integer WEEK_SUNDAY = 0;

    public static final Integer WEEK_MONDAY = 1;

    public static final Integer WEEK_TUESDAY = 2;

    public static final Integer WEEK_WEDNESDAY = 3;

    public static final Integer WEEK_THURSDAY = 4;

    public static final Integer WEEK_FRIDAY = 5;

    public static final Integer WEEK_SATURDAY = 6;
}
