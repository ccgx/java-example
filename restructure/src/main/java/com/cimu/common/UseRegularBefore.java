package com.cimu.common;

/**
 * 如何发挥正则表达式的威力
 */
public class UseRegularBefore {

    public boolean isMobile(String mobile) {
        if (mobile == null || "".equals(mobile)) {
            return false;
        } else if (mobile.length() != 11) {
            return false;
        } else if (!"1".equals(mobile.substring(0, 1))) {
            return false;
        } else {
            for (int i = 1; i <= 10; i++) {
                char everyNum = mobile.charAt(i);
                if (everyNum < 48 || everyNum > 57) {
                    return false;
                }
            }
        }
        return true;
    }

}

