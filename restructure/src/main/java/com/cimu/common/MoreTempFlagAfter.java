package com.cimu.common;

/**
 * 避免赋予临时变量过多的角色
 */
public class MoreTempFlagAfter {

    public static void method() {
        String tempType = "";
        String tempName = "";
        tempType = "it is " + getType();
        System.out.println(tempType);
        tempName = "this is " + getName();
        System.out.println(tempName);
    }

    private static String getName() {
        return "张三";
    }

    private static String getType() {
        return "aaaa";
    }

}

