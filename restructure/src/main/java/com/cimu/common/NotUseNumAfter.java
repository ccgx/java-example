package com.cimu.common;

/**
 * 避免使用魔法数字
 */
public class NotUseNumAfter {

    public static void method(int type) {
        if (type == Consts.NUM_3) {
            System.out.println("3");
        } else if (type == Consts.NUM_4) {
            System.out.println("4");
        }
    }
}

class Consts {

    public static int NUM_3 = 3;

    public static int NUM_4 = 4;
}