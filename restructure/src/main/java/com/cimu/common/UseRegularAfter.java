package com.cimu.common;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 如何发挥正则表达式的威力
 */
public class UseRegularAfter {

    public boolean validate(String regEx,Object value){
        if(regEx != null && value != null){
            Pattern pattern = Pattern.compile(regEx);
            Matcher matcher = pattern.matcher(value.toString());
            if(!matcher.find()){
                return false;
            }else{
                return true;
            }
        }
        return false;
    }

}

